@php
  $sample = isset( $section->tester ) ? 'Starter Theme | Is Set!' : 'Starter Theme | Not Set!';
@endphp

<h1>{{ $sample }} content-page.blade.php</h1>

<div class="test-image">
  <img src="@asset('images/vancouver-sample-image.jpeg')" />
</div>

@php the_content() @endphp
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}


