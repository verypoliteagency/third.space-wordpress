<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageTemplateNews extends Controller {

  public static function newsLoop( $related_posts = [] ) {

    $news_items = get_posts([
      'post_type' => 'post',
      'include' => $related_posts,
      'numberposts' => -1,
      'cat' => $newsCategory
    ]);

    return array_map( function ($post) {
      return [
        'pid' => $post->ID,
        'title' => apply_filters('get_the_title', $post->post_title),
        'excerpt' => get_the_excerpt($post->ID),
        'content' => apply_filters('the_content', $post->post_content),
        'date' => get_the_date('F j, Y', $post),
        'thumbnail' => get_post_thumbnail_id($post->ID),
        'link' => get_permalink($post->ID),
        'categories' => get_the_category( $post->ID ),
        'mini' => get_field('mini_content', $post->ID),
        'cta' => get_field('cta_button', $post->ID)
      ];
    }, $news_items );

  }

}
