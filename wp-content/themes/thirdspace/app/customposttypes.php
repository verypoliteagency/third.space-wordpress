<?php

namespace App;

use App\Controllers\App;

// add_action( 'init', function() {
// 	$labels = array(
// 		'name'                  => _x( 'Buildings', 'Post Type General Name', 'thirdspace' ),
// 		'singular_name'         => _x( 'Building', 'Post Type Singular Name', 'thirdspace' ),
// 		'menu_name'             => __( 'Buildings', 'thirdspace' ),
// 		'name_admin_bar'        => __( 'Buildings', 'thirdspace' )
// 	);
// 	$args = array(
// 		'label'                 => __( 'Building', 'thirdspace' ),
// 		'labels'                => $labels,
// 		'hierarchical'          => true,
// 		'public'                => true
// 	);
// 	register_post_type( 'building', $args );
// 	$labels = array(
// 		'name'                  => _x( 'Spaces', 'Post Type General Name', 'thirdspace' ),
// 		'singular_name'         => _x( 'Space', 'Post Type Singular Name', 'thirdspace' ),
// 		'menu_name'             => __( 'Spaces', 'thirdspace' ),
// 		'name_admin_bar'        => __( 'Spaces', 'thirdspace' )
// 	);
// 	$args = array(
// 		'label'                 => __( 'Space', 'thirdspace' ),
// 		'labels'                => $labels,
// 		'hierarchical'          => true,
// 		'public'                => true
// 	);
// 	register_post_type( 'space', $args );
// } );


// add_action( 'add_meta_boxes', function () {
// 	add_meta_box( 'space-parent', 'Building', function( $post ) {
//         $post_type_object = get_post_type_object( $post->post_type );
//         // if ( $post_type_object->hierarchical ) {
//             $pages = wp_dropdown_pages( array(
//             'post_type' => 'building',
//             'selected' => $post->post_parent,
//             'name' => 'parent_id',
//             'show_option_none' => __( '(no parent)' ),
//             'sort_column'=> 'menu_order, post_title',
//             'echo' => 0 ) );
//             if ( ! empty( $pages ) ) {
//                 echo $pages;
//             }
//         // }
//     }, 'space', 'side', 'default' );
// });


