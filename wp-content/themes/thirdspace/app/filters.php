<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
// add_filter('excerpt_more', function () {
//     return ' &hellip; <a href="' . get_permalink() . '">' . __('Read More', 'sage') . '</a>';
// });

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/*
//////////////////////////////////////////////////////////
////  Frank Strategy
//////////////////////////////////////////////////////////
*/

// ---------------------------------------- WYSIWYG Custom Formats
add_filter( 'tiny_mce_before_init', function ( $init_array ) {

    $style_formats = array(
      array(
          'title' => 'Blue Text',
          'inline' => 'span',
          'classes' => 'text-blue',
          'wrapper' => false,
      ),
      array(
          'title' => 'Red Text',
          'inline' => 'span',
          'classes' => 'text-red',
          'wrapper' => false,
      ),
      array(
          'title' => 'Yellow Text',
          'inline' => 'span',
          'classes' => 'text-yellow',
          'wrapper' => false,
      ),
      array(
          'title' => 'Paragraph Uppercase',
          'block' => 'span',
          'classes' => 'uppercase',
          'wrapper' => true,
      )
    );

    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

});

// ---------------------------------------- Dynamic Sidebar
add_filter( 'dynamic_sidebar_params' , function ( $params ) {

  if ($params[0]['name'] == "Footer") {
    $hLevel = '2';
  } else {
    $hLevel = '4';
  }

  $params[0]['before_title'] = '<h' . $hLevel . ' class="content-info--widget-title">' ;
  $params[0]['after_title']  = '</h' . $hLevel . '>' ;
  $params[0]['before_title'] = '<h4 class="content-info--widget-title">' ;
  $params[0]['after_title']  = '</h4>' ;

  return $params;

}, 100);

// ---------------------------------------- Gravity Forms
add_filter( 'gform_confirmation_anchor_3', '__return_false' );
