<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/*
//////////////////////////////////////////////////////////
////  Frank Strategy
//////////////////////////////////////////////////////////
*/

// ---------------------------------------- Console Logger
function console_log( $debugObj, $title = 'debug' ) {
  echo '<script>console.log("' . $title . '",';
  echo json_encode($debugObj);
  echo ')</script>';
}

// ---------------------------------------- Classname Builder
function get_settings_classname( $settings = [] ) {

  $className = '';

  if ( !empty( $settings ) ) {

    $colsXl = isset($settings->cols_xl) ? intval($settings->cols_xl) : false;
    $offsetXl = isset($settings->offset_xl) ? intval($settings->offset_xl) : false;
    $colsDesktop = isset($settings->cols_desktop) ? intval($settings->cols_desktop) : false;
    $offsetDesktop = isset($settings->offset_desktop) ? intval($settings->offset_desktop) : false;
    $colsTablet = isset($settings->cols_tablet) ? intval($settings->cols_tablet) : false;
    $offsetTablet = isset($settings->offset_tablet) ? intval($settings->offset_tablet) : false;
    $colsMobile = isset($settings->cols_mobile) ? intval($settings->cols_mobile) : false;
    $offsetMobile = isset($settings->offset_mobile) ? intval($settings->offset_mobile) : false;
    $bleedLeft = isset($settings->bleed_left) && $settings->bleed_left ? true : false;
    $bleedRight = isset($settings->bleed_right) && $settings->bleed_right ? true : false;
    $bleedLeftMobile = isset($settings->bleed_left_mobile) && $settings->bleed_left_mobile ? true : false;
    $bleedRightMobile = isset($settings->bleed_right_mobile) && $settings->bleed_right_mobile ? true : false;
    $hideOnMobile = isset($settings->hide_on_mobile) && $settings->hide_on_mobile ? true : false;
    $fullBleed = isset($settings->full_bleed) && $settings->full_bleed ? true : false;
    $fullBleedMobile = isset($settings->full_bleed_mobile) && $settings->full_bleed_mobile ? true : false;
    $fullBleedBkg = isset($settings->full_bleed_background) ? intval($settings->full_bleed_background) : false;
    $padSides = isset($settings->pad_sides) ? intval($settings->pad_sides) : false;
    $theme = isset($settings->theme) ? $settings->theme : false;

    $className = ($colsMobile != "" ? " col-" . $colsMobile . " col-sm-" . $colsMobile : "") .
                  ($offsetMobile != "" ? " offset-" . $offsetMobile . " offset-sm-" . $offsetMobile : "") .
                  ($colsTablet != "" ? " col-md-" . $colsTablet : "") .
                  ($offsetTablet != "" ? " offset-md-" . $offsetTablet : "") .
                  ($colsDesktop != "" ? " col-lg-" . $colsDesktop : "") .
                  ($offsetDesktop != "" ? " offset-lg-" . $offsetDesktop : "").
                  ($colsXl != "" ? " col-xl-" . $colsXl : "") .
                  ($offsetXl != "" ? " offset-xl-" . $offsetXl : "") .
                  ($bleedLeft ? " bleed-left" : "") .
                  ($bleedRight ? " bleed-right" : "") .
                  ($bleedLeftMobile ? " bleed-left-mobile" : "") .
                  ($bleedRightMobile ? " bleed-right-mobile" : "") .
                  ($hideOnMobile ? " d-none d-sm-block" : "") .
                  ($fullBleed ? " full-bleed" : "") .
                  ($fullBleedMobile ? " full-bleed-mobile" : "") .
                  ($fullBleedBkg ? " full-bleed-bkg" : "") .
                  ($padSides ? " pad-sides" : "") .
                  ($theme != "" ? " theme-" . $theme : "");

  }

  return trim( $className );

}

// ---------------------------------------- Not totally sure
function get_settings_styles( $settings = [], $basePct = 100, $containerId = "" ) {

  if ( !empty( $settings ) ) {

    $marginTop = isset( $settings->margin_top ) ? $settings->margin_top : '';
    $marginBottom = isset( $settings->margin_bottom ) ? $settings->margin_bottom : '';
    $paddingTop = isset( $settings->padding_top ) ? $settings->padding_top : '';
    $paddingBottom = isset( $settings->padding_bottom ) ? $settings->padding_bottom : '';
    // $paddingLeft = $settings->padding_left;
    // $paddingRight = $settings->padding_right;
    $marginTopMobile =  isset( $settings->margin_top_mobile ) ? $settings->margin_top_mobile : '';
    $marginBottomMobile =  isset( $settings->margin_bottom_mobile ) ? $settings->margin_bottom_mobile : '';
    $paddingTopMobile =  isset( $settings->padding_top_mobile ) ? $settings->padding_top_mobile : '';
    $paddingBottomMobile =  isset( $settings->padding_bottom_mobile ) ? $settings->padding_bottom_mobile : '';

    $styles = (strlen($marginTop) ? "margin-top:" . $marginTop / 12 * $basePct . "% !important;": "" ) .
              (strlen($marginBottom) ? "margin-bottom:" . $marginBottom / 12 * $basePct . "% !important;": "" ) .
              (strlen($paddingTop) ? "padding-top:" . $paddingTop / 12 * $basePct . "% !important;": "" ) .
              (strlen($paddingBottom) ? "padding-bottom:" . $paddingBottom / 12 * $basePct . "% !important;": "" ) ;
              // (strlen($paddingLeft) ? "padding-left:" . $paddingLeft / 12 * $basePct . "% !important;": "" ) .
              // (strlen($paddingRight) ? "padding-right:" . $paddingRight / 12 * $basePct . "% !important;": "" );

    if (strlen($containerId)) {
      $styles =  "#" . $containerId . "{" . $styles . "}";
      if (strlen($marginBottomMobile) || strlen($marginTopMobile) || strlen($paddingBottomMobile) || strlen($paddingTopMobile)) {
        $styles .=  "@media (max-width: 767.98px){" .
                    "#" . $containerId . "{" .
                    (strlen($marginTopMobile) ? "margin-top:" . $marginTopMobile / 12 * $basePct . "% !important;": "" ) .
                    (strlen($marginBottomMobile) ? "margin-bottom:" . $marginBottomMobile / 12 * $basePct . "% !important;": "" ) .
                    (strlen($paddingTopMobile) ? "padding-top:" . $paddingTopMobile / 12 * $basePct . "% !important;": "" ) .
                    (strlen($paddingBottomMobile) ? "padding-bottom:" . $paddingBottomMobile / 12 * $basePct . "% !important;": "" ) .
                    "}}";
      }
    }

  }

  return $styles;

}

// ---------------------------------------- Render Call to Actions
function render_ctas( $ctas = [], $ctaGroup = false ) {

    $ctaOutput = '';

    if ( $ctas ) {

      foreach( $ctas as $cta ) {

        $ctaClass = isset( $cta->button_style ) && !empty( $cta->button_style ) ? $cta->button_style : 'text';
        $ctaLabel = isset( $cta->label ) ? $cta->label : false;
        $ctaLink = false;
        $ctaTarget = '';
        $ctaType = isset( $cta->type ) ? $cta->type : 'not-set';

        switch ( $ctaType ) {
          case 'page':
            $ctaLink = isset( $cta->link_page ) ? $cta->link_page : false;
            break;
          case 'anchor':
            $ctaLink = isset( $cta->link_anchor ) ? '#' . $cta->link_anchor : false;
            break;
          case 'email':
            $ctaLink = isset( $cta->link_email ) ? 'mailto:' . $cta->link_email : false;
            break;
          case 'file':
            $ctaLink = isset( $cta->link_file->url ) ? $cta->link_file->url : false;
            $ctaTarget = isset( $cta->link_file->url ) ? ' target="_blank"' : '';
            break;
          case 'custom':
            $ctaLink = isset( $cta->link_custom->url ) ? $cta->link_custom->url : false;
            $ctaTarget = isset( $cta->link_custom->url ) ? ' target="' . $cta->link_custom->target  . '"' : '';
            break;
        };

        if ( $ctaLabel && $ctaLink ) {
          $ctaOutput .= '<a class="btn btn-' . $ctaClass . '" href="' . $ctaLink . '"' . $ctaTarget .'>' . $ctaLabel . '</a>';
        }

      }

    }

    if ( $ctaGroup ) {
      $ctaOutput = '<div class="btn-group">' . $ctaOutput . '</div>';
    }

    return $ctaOutput;

}

// ---------------------------------------- Get File Contents
function get_file_contents($asset) {

  $asset_url = asset_path($asset);

  if (fopen($asset_url, 'r')) {
    return file_get_contents($asset_url);
  } else {
    return 'Could not locate the file. Make sure it exists! Or try running "yarn build" again';
  }

}

// ---------------------------------------- Wrap Periods
function wrap_periods($text) {

  $search = '.';
  $replace = '<span class="period">.</span>';
  $result = str_replace($search, $replace, $text);
  // $result = strrev(implode(strrev($replace), explode(strrev($search), strrev($text), 2)));

  return $result;

}

// ---------------------------------------- Text Wrap Periods
function text_wrap_periods($text) {

  $search = '.</h';
  $replace = '<span class="period">.</span></h';
  $result = str_replace($search, $replace, $text);

  return $result;

}

// ---------------------------------------- Get Spaces by Parent ID and WP Query
function get_spaces($parentId) {

  $args = array(
      'post_type'      => 'page',
      'posts_per_page' => -1,
      'post_parent'    => $post->ID,
      'order'          => 'ASC',
      'orderby'        => 'menu_order'
  );

  return new WP_Query( $args );

}

// ---------------------------------------- Remove the parent custom post type slug from URL
add_filter( 'post_type_link', function( $post_link, $post ) {

  $uri = '';

  if ( 'space' != $post->post_type ) {
    return $post_link;
  }

  foreach ( $post->ancestors as $parent ) {
    $uri = get_post( $parent )->post_name . "/" . $uri;
  }

  return str_replace( $uri, '', $post_link );

}, 10, 2 );

// ---------------------------------------- Check Available (WHAT THO????)
function check_available() {

  $spaceIds = get_posts(array(
    'fields'          => 'ids',
    'posts_per_page'  => -1,
    'post_type' => 'space'
  ));

  foreach( $spaceIds as $spaceId ) {
    if ( get_field('available', $spaceId ) ) {
      return true;
    }
  }

}

// ---------------------------------------- Tru
function truncateToWord($text, $charLength = 100) {

  if ( strlen( $text) > $charLength ) {
    $text = explode( "\n", wordwrap( $text, $charLength));
    $text = $text[0] . '...';
  }

  return $text;

}

/*
//////////////////////////////////////////////////////////
////  Polite Dept.
//////////////////////////////////////////////////////////
*/

function debug_this( $object = null, $title = 'Name me!' ) {

  $html = '<div class="php-debugger" style="background: black; color: #fff; width: 90%; display: block; margin: 65px auto; padding: 40px 25px; position: relative; z-index: 10;">';
    $html .= '<pre style="color: #fff;">';
      $html .= '<h2>Debugger for: ' . $title . '</h2>';
	    $html .= '<h3>Variable is type:</h3>';
	    $html .= gettype ( $object );
	    $html .= '<h3 style="margin-top: 25px;">Variable contents:</h3>';
	    $html .= print_r( $object, true );
    $html .= '</pre>';
  $html .= '</div>';

  echo $html;

}

// ---------------------------------------- Classname Builder
function get_bleed_settings( $settings = false ) {

  $classes = '';

  if ( $settings ) {
    $classes .= isset( $settings->bleed_left ) && $settings->bleed_left ? 'bleed-left' : '';
    $classes .= isset( $settings->bleed_right ) && $settings->bleed_right ? 'bleed-right' : '';
    $classes .= isset( $settings->bleed_left_mobile ) && $settings->bleed_left_mobile ? 'bleed-left-mobile' : '';
    $classes .= isset( $settings->bleed_right_mobile ) && $settings->bleed_right_mobile ? 'bleed-right-mobile' : '';
  }

  return trim( $classes );

}

// ---------------------------------------- Render Container
function render_container( $state = 'open', $col = 'col-12', $container = 'container' ) {

  $html = '<div class="' . $container . '"><div class="row"><div class="' . $col . '">';
  if ( 'open' !== $state ) {
    $html = '</div></div></div>';
  }
  return $html;

}

// ---------------------------------------- Render Call to Action Link
function render_cta_link( $cta = [], $classes = '' ) {

    $html = '';

    if ( $cta ) {

      $title = $type = $url = $url_page = $url_post = $url_external = false;
      $rel = false;
      $style = 'not-set';
      $target = '_self';
      extract( $cta );

      switch( $type ) {
        case 'external':
          $url = $url_external ? $url_external : '';
          $target = '_blank';
          $rel = 'rel="noopener"';
          break;
        case 'page':
          $url = get_permalink( $url_page ) ? get_permalink( $url_page ) : '';
          break;
        case 'post':
          $url = get_permalink( $url_post ) ? get_permalink( $url_post ) : '';
          break;
      }

      if ( $url && $title ) {
        $html = '<a
          class="cta__link link ' . ( $classes ? $classes : '' ) . '"
          data-style="' . $style . '"
          href="' . $url . '"
          target="' . $target . '"
          ' . ( $rel ? $rel : '' ) . '
        >';
          $html .= '<span class="link__title">' . $title . '</span>';
          $html .= ( 'arrow' == $style ) ? '<span class="link__icon arrow">' . template('svgs.icon-arrow') . '</span>' : '';
        $html .= '</a>';
      }

    }

    return $html;

}

// ---------------------------------------- Render Call to Action Link
function render_wp_navigation( $menu_title = 'Main Navigation', $current_post_id = false ) {

  $html = '';

  if ( wp_get_nav_menu_items ( $menu_title ) ) {

    $menu_items = wp_get_nav_menu_items ( $menu_title );

    // loop through menu object here, only output '<li>' elements
    foreach ( $menu_items as $item ) {

      // default data
      $id = $link = $title = $type = false;
      $classes = '';

      $id = $item->object_id;
      $link = $item->url;
      $title = $item->title;
      $type = $item->object;
      $target = '_self';
      $rel = false;

      if ( $id == $current_post_id ) {
        $classes .= 'active';
      }
      if ( 'custom' == $type ) {
        $target = '_blank';
      }

      $html .= '<div
        class="navigation__item' . ( $classes ? ' ' . $classes : '' ) . '"
        data-this-type="' . $type . '"
        data-current-id="' . $current_post_id . '"
        data-link-id="' . $id . '"
      >';
        $html .= '<a class="navigation__link" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      $html .= '</div>';

    }

  }

  return $html;

}
