<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most template systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views.
    |
    */

    'paths' => [
        get_theme_file_path().'/resources/views',
        get_parent_theme_file_path().'/resources/views',
    ],


    /*
    |--------------------------------------------------------------------------
    | Compiled View Path
    |--------------------------------------------------------------------------
    |
    | This option determines where all the compiled Blade templates will be
    | stored for your application. Typically, this is within the uploads
    | directory. However, as usual, you are free to change this value.
    |
    */

    // 'compiled' => wp_upload_dir()['basedir'].'/cache', // Default
    'compiled' => '/tmp/sage-cache', // WP Engine Fix
    // 'compiled' => '/tmp/sage-cache/thirdspacedev',  // Incorrect WP Engine Fix

    /*

      "So normal tech’s don’t have the permissions to create the folder where it needs to be made. So you’ll need to request to be escalated.
      You can just say that we need to set up Sage/Blade compiler cache files, and I need to be escalated for this."

      You can simply state there is a folder on the root `/tmp/sage-cache/thirdspacedev` that you are using for Sage Builder Themes.  This should get our tech in the right path. I do apologize for the trouble previously.

    */

    /*
    |--------------------------------------------------------------------------
    | View Namespaces
    |--------------------------------------------------------------------------
    |
    | Blade has an underutilized feature that allows developers to add
    | supplemental view paths that may contain conflictingly named views.
    | These paths are prefixed with a namespace to get around the conflicts.
    | A use case might be including views from within a plugin folder.
    |
    */

    'namespaces' => [
        /* Given the below example, in your views use something like: @include('WC::some.view.or.partial.here') */
        // 'WC' => WP_PLUGIN_DIR.'/woocommerce/templates/',
    ],
];
