// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import singleBuilding from './routes/singleBuilding';
import singleSpace from './routes/singleSpace';
import contact from './routes/contact';
import single from './routes/single';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // singleBuilding template
  singleBuilding,
  // singleSpace template
  singleSpace,
  // contact template
  contact,
  // single template
  single,
});
// Load Events
jQuery(document).ready(() => routes.loadEvents());
