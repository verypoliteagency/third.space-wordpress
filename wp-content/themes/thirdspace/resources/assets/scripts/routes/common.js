import 'slick-carousel/slick/slick.min';
import AOS from 'aos';

export default {
  init() {

    // JavaScript to be fired on all pages
    AOS.init({
      duration: 350,
      initClassName: 'aos-initialized',
    });

  },
  finalize() {

    // JavaScript to be fired on all pages, after page specific JS is fired
    $('.section').each((index, element) => {
      let pCount = 1;
      $(element).find('.period').each((index, element) => {
        $(element).attr('data-period-count', pCount)
        pCount = pCount < 3 ? pCount + 1 : 1
      })

    })

    function createModal() {
      const $modal = $(`
        <div class="modal fade modal--popup" id="popupModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content wrap">
              <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="material-icons">close</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5 bleed-left modal-body-image"></div>
                    <div class="col-sm-7 col-md-6 offset-md-1 col-lg-5 my-auto modal-body-content">
                      <div class="modal-body-header"></div>
                      <div class="offset-md-2 modal-body-detail"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>`)

      return $modal
    }

    function populatePopupModal($targetItem, $modal) {
      const $targetImage = $targetItem.find('.js-popup-image-large').length ? $targetItem.find('.js-popup-image-large') : $targetItem.find('.js-popup-image')
      $targetImage.clone().appendTo($modal.find('.modal-body-image'))
      $targetItem.find('.component--popup-subtitle').clone().prependTo($modal.find('.modal-body-header'))
      $targetItem.find('.component--popup-title').clone().prependTo($modal.find('.modal-body-header'))
      $modal.find('.modal-body-detail').html($targetItem.find('.component--popup-detail').html())
      $modal.find('.d-none').removeClass('d-none')
    }

    $('.component--popup-btn')
      .on('click', (e) => {
        const $targetItem = $($(e.currentTarget).attr('data-popup-target'));
        const $popupModal = createModal()
        $('body').append($popupModal)
        populatePopupModal($targetItem, $popupModal)
        $popupModal.modal().on('hidden.bs.modal', function () {
          $popupModal.remove()
        })
      })
      .hover( (e) => {
        $($(e.currentTarget).attr('data-popup-target')).toggleClass('hover').siblings().removeClass('hover')
      })

    const settings = {
      autoplay: false,
      autoplaySpeed: 2000,
      pauseOnFocus: true,
      pauseOnHover: true,
      dots: false,
      arrows: true,
      touchThreshold: 300,
      swipeToSlide: true,
      variableWidth: true,
      infinite: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            // variableWidth: false,
            // infinite: true,
            slidesToShow: 1,
          },
        },
      ],
    }
    $('.js-gallery').slick( settings )

    function getUrlVars(sParam)
    {
        const sPageURL = window.location.search.substring(1);
        const sURLVariables = sPageURL.split('&');
        for (let i = 0; i < sURLVariables.length; i++)
        {
            const sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }

    // REMOVE FOR PRODUCTION!
    $(document).on('keydown', (e) => {
      if (e.ctrlKey && e.key === 'g') {
        $('.grid-guides').toggle()
      }
    })

    if($('body.single-building').length) {
      const buildingType = getUrlVars('type')
      // console.log('buildingType', buildingType)
      if(buildingType) {
        $('.breadcrumb-item.active a').click((e) => {
          e.preventDefault()
          window.location.replace($(e.currentTarget).attr('href') + '?type=' + buildingType)
        })
      }
    }

    function setQueryStringParameter(name, value) {
      const params = new URLSearchParams(window.location.search)
      params.set(name, value)
      window.history.replaceState({}, '', decodeURIComponent(`${window.location.pathname}?${params}`))
    }

    if($('body.our-spaces').length) {
      const buildingType = getUrlVars('type')
      //console.log('buildingType', buildingType)
      if(buildingType) {
        showFiltered(buildingType)
      }
    }

    function showFiltered(filterType) {
      $(`.listing[data-listing-type*="${filterType}"]`)
            .show()
            .find('.our-spaces--link').each((index,el) => {
              $(el).click((e) => {
                e.preventDefault()
                window.location.replace($(e.currentTarget).attr('href') + '?type=' + filterType)
              })
            })
          $('.listing').not(`[data-listing-type*="${filterType}"]`).hide()
    }

    $('.js-btn-filter')
      .on('click', (e) => {
        e.preventDefault()
        const filterType = $(e.currentTarget).attr('data-filter-type')
        // console.log('filtertype',filterType)
        if (filterType != 'all') {
          setQueryStringParameter('type', filterType)
          showFiltered(filterType)
        } else {
          $('.listing').show()
          window.history.replaceState(null, null, window.location.pathname)
        }
        // filterOn = true
      })

    function isInViewport(element) {
      const elementTop = element.offset().top
      const elementBottom = elementTop + element.outerHeight()

      const viewportTop = $(window).scrollTop()
      const viewportBottom = viewportTop + $(window).height()

      return elementBottom > viewportTop && elementTop < viewportBottom
    }

    function navColor() {
      if($('.page-header .component--banner').hasClass('theme-dark')) {
        $('.banner, .header.sticky').addClass('theme-dark')
      }
    }

    navColor()

    function scrollFade() {
      $('.component--popup-image-ctn, .news-listing--image-ctn, .component--image-image, .our-spaces--thumbnail-ctn, .find-a-space--thumbnail-ctn').each( (index, element) => {
        // $(element)
        //   .addClass('image-fade')

        if(isInViewport($(element))) {
          $(element).addClass('active').attr('decoding:', 'async')
        } else {
          $(element).removeClass('active')
        }
      })

      $('.news-listing--details, .component--text-content, .our-spaces--details, .find-a-space--details, .home .component--cards-card').each( (index, element) => {
        // $(element)
        //   .addClass('text-fade')

        if(isInViewport($(element))) {
          $(element).addClass('active').attr('decoding:', 'async')
        } else {
          $(element).removeClass('active')
        }
      })
    }

    function isWindowScrolled() {
      const offset = window.innerWidth > 576 ? 90 : window.innerHeight * 0.6
      const scroll = $(window).scrollTop()
      if(scroll > offset) {
        $('body').addClass('scrolled test')
      } else {
        $('body').removeClass('scrolled test')
      }

      if(scroll > 5) {
        $('body').addClass('hero-scrolled')
      } else {
        $('body').removeClass('hero-scrolled')
      }
    }

    let previousScroll = 0

    function getScrollDirection(){
      const currentScroll = $(window).scrollTop()
      if (currentScroll > previousScroll){
        $('body').attr('data-scroll-direction', 'down')
      } else {
        $('body').attr('data-scroll-direction', 'up')
      }
      previousScroll = currentScroll
    }


    window.setTimeout(() => scrollFade(), 100);

    isWindowScrolled()
    getScrollDirection()

    $(window).on('scroll', () => {
      scrollFade()
      isWindowScrolled()
      getScrollDirection()
    })

    function viewportHeightFix() {
      // iOS viewport fix - First we get the viewport height and we multiple it by 1% to get a value for a vh unit
      let vh = window.innerHeight * 0.01
      let vw = $(window).width() * 0.00999999
      // Then we set the value in the --vh custom property to the root of the document
      document.documentElement.style.setProperty('--vh', `${vh}px`)
      document.documentElement.style.setProperty('--vw', `${vw}px`)
    }
    viewportHeightFix()


    //////////////////////////////////////////////////////////
    ////  Polite Dept.
    //////////////////////////////////////////////////////////

    let scrollTicker, resizeTicker;
    scrollTicker = resizeTicker = false;

    // ---------------------------------------- Set CSS Variable
    const setCSSVariable = ( $id = '', $value = '' ) => {
      if ( $id && $value ) {
        document.documentElement.style.setProperty( '--' + $id, $value + 'px' );
      }
    };

    // ---------------------------------------- Get element height by tag
    const getElementHeightByTag = ( $tag = '' ) => {
      let element = document.getElementsByTagName( $tag )[0] || false;
      if ( element ) {
        return element.offsetHeight;
      }
      return 0;
    };

    // ---------------------------------------- Is in View
//     function isInView( el = false ) {
//
//       let inView = false;
//
//       if ( el ) {
//
//         let elRect = el.getBoundingClientRect();
//
//         let elTop = el.offsetTop;
//         let elBottom = elTop + el.offsetHeight;
//         let viewportTop = window.scrollY || window.pageYOffset;
//         let viewportBottom = viewportTop + window.innerHeight;
//
//         inView = ( elRect.top >= 0 && elRect.left >= 0 && elRect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && elRect.right <= (window.innerWidth || document.documentElement.clientWidth ) );
//         inView = ( elBottom > viewportTop && elTop < viewportBottom );
//
//         // console.log( 'isInView(el) ::', {
//         //   inView,
//         //   viewportTop,
//         //   viewportBottom,
//         // });
//
//       }
//
//       return inView;
//
//     }

    // ---------------------------------------- Set Scroll State
    function setScrollState() {

      // let inView = isInView();

      // ( document.querySelectorAll( '.js--fade-in-when-in-view' ) || [] ).forEach((element) => {
      //   if ( isInView( element ) ) {
      //     element.classList.add('nu-active');
      //   } else {
      //     element.classList.remove('nu-active');
      //   }
      // });

      // console.log( 'setScrollState() fired!', { inView } );

    }

    // ---------------------------------------- Set Scroll State
    function setHeaderHeightToCSSVar() {
      let headerHeight = getElementHeightByTag('header');
      if ( headerHeight ) {
        setCSSVariable( 'theme-header-height-total', headerHeight );
      }
    }

    $('.js--menu-trigger-button').on('click', function() {
      $('.header__collapse-navigation').toggleClass('active');
      $('body').toggleClass('collapse-navigation-active');
    });

    setScrollState();
    setHeaderHeightToCSSVar();

    // ---------------------------------------- On Scroll
    document.addEventListener( 'scroll', function() {
      if ( !scrollTicker ) {
        window.requestAnimationFrame(function() {
          // execute throttled stuff...
          setScrollState();
          setHeaderHeightToCSSVar();
          scrollTicker = false;
        });
        scrollTicker = true;
      }
    });

    // ---------------------------------------- On Resize
    window.addEventListener('resize', function(){
      if ( !resizeTicker ) {
        window.requestAnimationFrame(function() {
          // execute throttled stuff...
          setHeaderHeightToCSSVar();
          resizeTicker = false;
        });
        resizeTicker = true;
      }
    });

    const localStorageKey = {
      service: 'THIRD--seen-service-cta',
    };

    // // ---------------------------------------- Services CTA
    // function getPathName() {
    //   return window.location.pathname || '';
    // }

    // ---------------------------------------- Toggle ServiceCTA
    function toggleServiceCta( $state = 'show' ) {

      let state = $state;
      let element = document.getElementById('services-cta') || false;
      let acceptedStates = [ 'show', 'close' ].includes( state );

      if ( element && acceptedStates ) {
        updateServicesCtaLocalStorage()
        switch( state ) {
          case 'show': {
            let delay = element.dataset.delay || 2500;
            let duration = element.dataset.duration || 5500;
            setTimeout(function(){
              element.classList.add('active');
              element.addEventListener('transitionend', () => {
                setTimeout(function(){
                  element.classList.remove('active');
                }, duration );
              });
            }, delay );
            break;
          }
        }
      }

    }

    // ---------------------------------------- Update Services CTA Local Storage
    function updateServicesCtaLocalStorage() {

      let now = Date.now();
      let seenServiceCta = localStorage.getItem( localStorageKey.service ) || false;
      let timeDifference = {
        days: 0,
        hours: 0,
      };
      let timeLimit = {
        days: 1,
        hours: 24,
      };

      if ( seenServiceCta ) {
        // NOT first time
        seenServiceCta = JSON.parse(seenServiceCta);
        timeDifference.days = (now.valueOf() - seenServiceCta.date.valueOf()) / (24*60*60*1000);
        timeDifference.hours = (now.valueOf() - seenServiceCta.date.valueOf()) / (60*60*1000);

        // if time difference between now and first, is more than time limit
        if ( timeDifference.days > timeLimit.days ) {
          // reset local storage to date now, and count 1
          localStorage.setItem(localStorageKey.service, JSON.stringify({
            date: now,
            count: 1,
          }));
        } else {
          // increment count by 1 and then update local storage
          seenServiceCta.count = seenServiceCta.count + 1;
          localStorage.setItem(localStorageKey.service, JSON.stringify(seenServiceCta));
        }

      } else {
        // first time, set new date and count to 1
        localStorage.setItem(localStorageKey.service, JSON.stringify({
          date: now,
          count: 1,
        }));
      }

    }

    //let seenServiceCtaCount = 1;
    // if ( localStorage.getItem( localStorageKey.service ) ) {
    //   seenServiceCtaCount = JSON.parse(localStorage.getItem( localStorageKey.service )).count || 1;
    // }
    // let currentPath = getPathName().replace(/^\/|\/$/g, '');
    // let pathsToSkip = [ 'services', 'service' ];
    // let pathOk = ( pathsToSkip.indexOf( currentPath ) > -1 ) ? false : true;
    let pathOk = ( location.pathname == '/' ) ? true : false;
    let referrer = document.referrer;
    let hostreference = location.href;
    // console.log( 'location :: ', location );
    // console.log( 'document :: ', document );
    // console.log( typeof document.referrer );
    let referrerOk = ( referrer === '' || hostreference == referrer ) ? true : false;

    console.log({ pathOk, referrerOk, hostreference, referrer  });

    if ( pathOk && referrerOk ) {
      toggleServiceCta('show');
    }

  },
};
