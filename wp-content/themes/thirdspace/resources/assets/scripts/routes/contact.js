
export default {
  init() {
    // JavaScript to be fired on the about us page
    // let filterOn = false;
    $('.gfield_label')
    .on('click', (e) => {
      $(e.currentTarget).siblings('.ginput_container input, .ginput_container textarea').trigger('focus')
    })

  $('.ginput_container input, .ginput_container textarea')
    .on('focus', (e) => {
      $(e.currentTarget).closest('.gfield').addClass('focus')
    })
    .on('blur', (e) => {
      $(e.currentTarget).closest('.gfield').removeClass('focus')
    })
    .on('change', (e) => {
      console.log('typing', $(e.target).val().length)
      $(e.target).val().length ? $(e.currentTarget).closest('.gfield').addClass('filled') : $(e.currentTarget).closest('.gfield').removeClass('filled')
    })

  },
}
