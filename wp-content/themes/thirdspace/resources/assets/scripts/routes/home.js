const Vimeo = window.Vimeo;

export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    // function videoResize($video) {
    //   const $section = $video.closest('.section')
    //   if ($section.width() < $section.height() * 640 / 360) {
    //     $video.css({
    //       width: $section.height() * 640 / 360,
    //       height: $section.height(),
    //     })
    //   } else {
    //     $video.css({
    //       width: $section.width(),
    //       height: $section.width() * 360 / 640,
    //     })
    //   }
    // }

    if ($('.section--bkg-video-ctn').length) { //if video bkg exists
      const $videoCtn = $('.section--bkg-video-ctn')
      const videoUrl = $videoCtn.attr('data-video-url')
      // const vimeoRegex = /(http|https)?:\/\/(www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^/]*)\/videos\/|)(\d+)(?:|\/\?)/
      if (videoUrl.length) {
        // const videoId = videoUrl.match(vimeoRegex)[4]
        // const cleanUrl = 'https://vimeo.com/' + videoId
        // console.log('vimeo', cleanUrl)
        const options = {
          url: videoUrl,
          background: true,
          autoplay: true,
          byline: false,
          loop: true,
          title: false,
          controls: false,
          width: '100%',
        }

        const videoPlayer = new Vimeo.Player('videoBkg', options)

        console.log('videoPlayer', $(videoPlayer.element).children())

        videoPlayer.on('play', function() {
          console.log('Played the video')
        })
      }
    }
  },
};
