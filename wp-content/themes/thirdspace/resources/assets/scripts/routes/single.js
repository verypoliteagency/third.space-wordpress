
export default {
  init() {
    // JavaScript to be fired on the about us page
    // let filterOn = false;
    $('.awsm-job-form-group label')
      .on('click', (e) => {
        $(e.currentTarget).siblings('.awsm-job-form-field').trigger('focus')
      })

  $('input.awsm-job-form-field')
    .on('focus', (e) => {
      console.log('hi')
      $(e.currentTarget).closest('.awsm-job-form-group').addClass('focus')
    })
    .on('blur', (e) => {
      $(e.currentTarget).closest('.awsm-job-form-group').removeClass('focus')
    })
    .on('change', (e) => {
      console.log('typing', $(e.target).val().length)
      $(e.target).val().length ? $(e.currentTarget).closest('.awsm-job-form-group').addClass('filled') : $(e.currentTarget).closest('.awsm-job-form-group label').removeClass('filled')
    })
  },
}
