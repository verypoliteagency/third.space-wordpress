import 'slick-carousel/slick/slick.min'

export default {
  init() {
    // JavaScript to be fired on the about us page

    function createSpaceModal() {
      const $modal = $(`
        <div class="modal fade modal--popup" id="popupModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content wrap">
              <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="material-icons">close</span>
                </button>
              </div>
              <div class="modal-body">
              </div>
            </div>
          </div>
        </div>`)

      return $modal
    }



    function pageLoad(pageUrl, $modal) {
      $.ajax({
        url: pageUrl + '&dynamic=1',
        dataType: 'html',
        method: 'POST',
      }).done(function (html) {
        $modal.find('.modal-body').append(html)

        const settings = {
          autoplay: false,
          autoplaySpeed: 2000,
          pauseOnFocus: true,
          pauseOnHover: true,
          dots: false,
          arrows: true,
          infinite: false,
          variableWidth: true,
        }
        $modal.find('.js-gallery').slick( settings )
      })
    }

    $('.space-listing--link')
      .click((e) => {
        e.preventDefault()
        const $targetUrl = $(e.currentTarget).attr('data-modal-url');
        const $spaceModal = createSpaceModal()
        pageLoad($targetUrl, $spaceModal)
        $('body').append($spaceModal)
        $spaceModal.modal().on('hidden.bs.modal', function () {
          $spaceModal.remove()
        })
      })
      .hover((e) => {
        $($(e.currentTarget).attr('data-popup-target')).toggleClass('hover')
      })
  },
};
