@extends('layouts.app')

@php

  // ---------------------------------------- Vars
  $partial_name = '404.blade.php';
  $acf_options = isset( $acf_options ) ? $acf_options : false;
  $component_banner = isset( $acf_options->component_banner ) ? $acf_options->component_banner : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $component_banner, '$component_banner' );
  }

@endphp

@section('content')
  @include( 'partials.page-header', [ 'heading_tag' => 'h1', 'component_banner' => $component_banner ])
@endsection
