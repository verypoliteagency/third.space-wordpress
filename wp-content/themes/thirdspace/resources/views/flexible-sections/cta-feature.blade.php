@php
  // ---------------------------------------- Vars
  $content = isset( $content ) && !empty($content) ? $content : [];
  $content_cta_list = isset( $content['cta_list'] ) ? $content['cta_list'] : [];
  $content_cta_list_count = count( $content_cta_list );
  $content_heading = isset( $content['heading'] ) ? $content['heading'] : '';
  $content_heading_tag = isset( $index ) && 1 == $index ? 'h1' : 'h2';
  $container = isset( $container ) && $container ? true : false;
@endphp

@if( $content )
  <div class='{{ $block_name }}__main'>
    {!! $container ? App\render_container( 'open', 'col-12', 'container-fluid' ) : '' !!}

      @if( $content_heading )
        <{!! $content_heading_tag !!} class="{{ $block_name }}__heading heading">{!! $content_heading !!}</{{ $content_heading_tag }}>
      @endif

      @if( $content_cta_list )
        <div class='{{ $block_name }}__grid' data-item-count="{{ $content_cta_list_count }}">
          <div class='row row--inner'>
            @foreach( $content_cta_list as $item )

              @php
                $heading = isset($item['heading']) ? $item['heading'] : '';
                $image_id = isset($item['image']['id']) ? $item['image']['id'] : [];
                $link = isset($item['link']) ? $item['link'] : [];
                $columns = 'col-12';
                switch( $content_cta_list_count ) {
                  case 2:
                    $columns .= ' col-lg-6';
                    break;
                  case 3:
                    $columns .= ' col-lg-4';
                    break;
                }
              @endphp

              @if( $image_id )
               <div class='{{ $columns }}'>
                  <div class='{{ $block_name }}__item'>
                    @if( $heading || $link )
                      <div class='{{ $block_name }}__item-content'>
                        {!! $heading ? '<h3 class="' . $block_name . '__item-heading heading" data-aos="fade-in" data-aos-delay="250">' . $heading . '</h3>'  : '' !!}
                        {!! $link ? '<div class="' . $block_name . '__item-cta cta" data-aos="fade-in" data-aos-delay="500">' . App\render_cta_link( $link ) . '</div>'  : '' !!}
                      </div>
                    @endif
                    <div class="{{ $block_name }}__item-image image" data-aos="fade-in">{!! wp_get_attachment_image( $image_id, "full", "", [] ) !!}</div>
                  </div>
                </div>
              @endif

            @endforeach
          </div>
        </div>
      @endif
    {!! $container ? App\render_container( 'closed' ) : '' !!}
  </div>
@endif
