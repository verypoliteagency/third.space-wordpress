@php
  // ---------------------------------------- Vars
  $content = isset( $content ) && !empty($content) ? $content : [];
  $content_image_list = isset( $content['image_list'] ) ? $content['image_list'] : [];
  $content_image_list_count = count( $content_image_list );
  $container = isset( $container ) && $container ? true : false;
@endphp

@if( $content )
  <div class='{{ $block_name }}__main'>
    {!! $container ? App\render_container( 'open', 'col-12', 'container' ) : '' !!}
      @if( $content_image_list )
        <div class='{{ $block_name }}__grid' data-item-count="{{ $content_image_list_count }}">
          @foreach( $content_image_list as $item )

            @php
              $image_id = isset($item['image']['id']) ? $item['image']['id'] : [];
              $image_class = $block_name . "__image image image-";
              $image_class .= $loop->iteration > 10 ? $loop->iteration : '0' . $loop->iteration;
            @endphp

            @if( $image_id )
              <div class='{{ $block_name }}__item' data-aos="fade-in" data-aos-duration="550">
                {!! wp_get_attachment_image( $image_id, "full", "", [ "class" => $image_class ] ) !!}
              </div>
            @endif

          @endforeach
        </div>
      @endif
    {!! $container ? App\render_container( 'closed' ) : '' !!}
  </div>
@endif
