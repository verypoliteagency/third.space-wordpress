@php
  // ---------------------------------------- Vars
  $content = isset( $content ) && !empty($content) ? $content : [];
  $container = isset( $container ) && $container ? true : false;
  $delay = isset( $content['heading'] ) && !empty($content['heading']) ? '350' : '0';
@endphp

@if( $content )
  <div class='{{ $block_name }}__main'>
    {!! $container ? App\render_container( 'open', 'col-12', 'container-fluid' ) : '' !!}
    {!! isset( $content['heading'] ) && !empty($content['heading']) ? '<h2 class="' . $block_name . '__heading heading" data-aos="fade-in">' . $content['heading'] . '</h2>'  : '' !!}
    {!! isset( $content['message'] ) && !empty($content['message']) ? '<div class="' . $block_name . '__message message" data-aos="fade-in" data-aos-delay="' . $delay . '">' . $content['message'] . '</div>'  : '' !!}
    {!! $container ? App\render_container( 'closed' ) : '' !!}
  </div>
@endif
