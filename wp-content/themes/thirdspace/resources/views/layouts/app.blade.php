@php

  // ---------------------------------------- Vars
  $partial_name = 'app.blade.php';
  $request = isset($_GET["dynamic"]) && !empty($_GET["dynamic"]) ? $_GET["dynamic"] : false;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $request, '$request' );
  }

@endphp

@if ( !$request )
  <!doctype html>
  <html {!! get_language_attributes() !!}>
    @include('partials.head')
    @php do_action('get_header') @endphp
    <body data-status="updated" @php body_class() @endphp>
      @include('partials.header')
      <div class="wrap" role="document">
        <div class="content">
          <main class="main">
            @yield('content')
          </main>
        </div>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @include('partials.services-pop-up')
      @php wp_footer() @endphp
    </body>
  </html>
@else
  <div class="to-modal js-to-modal">
    @yield('content')
  </div>
@endif
