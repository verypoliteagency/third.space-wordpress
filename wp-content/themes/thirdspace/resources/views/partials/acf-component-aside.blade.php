@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-aside.blade.php';
  $aside = isset( $aside ) ? $aside : [];
  $aside_image = isset( $aside->image ) ? $aside->image : [];
  $aside_image_id = isset( $aside_image->id ) ? $aside_image->id : false;
  $aside_image_placement = isset( $aside->image_placement ) ? $aside->image_placement : 'not-set';
  $aside_text = isset( $aside->text ) ? $aside->text : '';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $aside_text )

  @if ( $aside_image_id )
    <div class="component--aside-image-ctn position-{{ $aside_image_placement }}">
      {!! wp_get_attachment_image( $aside_image_id, "full", "", [ "class" => "component--aside-image" ] ) !!}
    </div>
  @endif

  <div class="component--aside-text">{!! $aside_text !!}</div>

@endif
