@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-banner.blade.php';

  $component_id = 'component_' . uniqid();
  $component_banner = isset( $component_banner ) ? $component_banner : false;
  $component_banner_bg_colour = isset( $component_banner->background_color ) ? $component_banner->background_color : 'not-set';
  $component_banner_bg_image = isset( $component_banner->background_image ) ? $component_banner->background_image : [];
  $component_banner_bg_video = isset( $component_banner->background_video ) ? $component_banner->background_video : false;
  $component_banner_theme = isset( $component_banner->theme ) ? $component_banner->theme : 'not-set';
  $component_banner_title = isset( $component_banner->title ) ? $component_banner->title : 'Title Not Set';
  $component_classnames = $component_banner && isset( $component_banner->layout_settings ) ? App\get_settings_classname( $component_banner->layout_settings ) : '';
  $component_styles = $component_banner && isset( $component_banner->layout_settings ) ? App\get_settings_styles( $component_banner->layout_settings, 100, $component_id ) : '';
  $heading_tag = isset( $heading_tag ) ? $heading_tag : 'h1';

  // #BUGGY - what is this
  $components = isset( $component_banner->content ) ? $component_banner->content : false;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $component_banner, '$component_banner' );
  }

@endphp

<div class="test"></div>

@if ( $component_banner and !empty( $component_banner ) )
  <div data-partial="{!! $partial_name !!}">

    @if ( is_front_page() )
      <div class="section--bkg col-12 theme-{!! $component_banner_theme !!} bkg-{!! $component_banner_bg_colour !!}">

        @if ( !empty( $component_banner_bg_image ) )
          <div class="section--bkg-image-ctn">
            {!! wp_get_attachment_image( $component_banner_bg_image->id, "full", "", [ "class" => "component--banner-image" ] ) !!}
          </div>
        @endif

        @if ( !empty( $component_banner_bg_video ) )
          <div class="section--bkg-video-ctn" id="videoBkg" data-video-url="{!! $component_banner_bg_video !!}"></div>
        @endif

      </div>
    @endif

    @if ( $component_styles )
      <style>{!! $component_styles !!}</style>
    @endif

    <div id="{!! $component_id !!}" class="component--banner theme-{!! $component_banner_theme !!} {!! empty( $component_banner_bg_image ) ? '' : 'has-bkg-image' !!}">

      <div class="component--banner-header">
        @if ( !empty( $component_banner_bg_image ) && !is_front_page() )
          <div class="component--banner-image-ctn">
            {!! wp_get_attachment_image( $component_banner_bg_image->id, "full","", ["class" => "component--banner-image"] ) !!}
          </div>
        @endif
        @if ( $component_banner_title )
          <div class="row">
            <{{ $heading_tag }} class="component--banner-title col offset-lg-1 col-lg-7">{!! App\wrap_periods( $component_banner_title ) !!}</{{ $heading_tag }}}>
          </div>
        @endif
      </div>

      <div class="component--banner-content row">
        @include( 'partials.acf-components', [ 'component_styles' => $component_styles, 'components' => $components ] )
      </div>

    </div>

  </div>
@endif
