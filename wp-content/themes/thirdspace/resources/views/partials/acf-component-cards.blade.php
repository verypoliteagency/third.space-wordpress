@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-cards.blade.php';
  $cards = isset( $cards ) ? $cards : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $cards )
  <div data-partial="{!! $partial_name !!}" class="row">
    @foreach( $cards as $card )

      @php

        $card_ctas = isset( $card->component_ctas ) ? $card->component_ctas : [];
        $card_description = isset( $card->description ) ? $card->description : '';
        $card_image = isset( $card->image ) ? $card->image : [];
        $card_title = isset( $card->title ) ? $card->title : '';

      @endphp

      <div data-aos="fade-in" data-aos-delay="{{ $loop->iteration * 250 }}" class="component--cards-card card text-fade col-12 col-md-4">

        @if ( $card_image )
          <div class="component__image card__image">
            {!! wp_get_attachment_image( $card_image->id, "full", "", [ "class" => "component--cards-img card-img-top" ] ) !!}
          </div>
        @endif

        <div class="component--cards-body card-body">
          @if ( $card_title )<h3 class="component--cards-title card-title">{!! $card_title !!}</h3>@endif
          @if ( $card_description )<div class="component--cards-text card-text">{!! $card_description !!}</div>@endif
          @if ( $card_ctas )
            <div class="component--cards-ctas">{{ App\render_ctas( $card_ctas ) }}</div>
          @endif
        </div>

      </div>

    @endforeach
  </div>
@endif

