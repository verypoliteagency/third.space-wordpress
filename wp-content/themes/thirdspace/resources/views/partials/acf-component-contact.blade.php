@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-contact.blade.php';
  $all = isset( $component->display ) && $component->display == "all" ? true : false;
  $contacts = isset( $contacts ) ? $contacts : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    // App\debug_this( $all, '$all' );
    // App\debug_this( $contacts, '$contacts' );
  }

@endphp

<div data-partial="{!! $partial_name !!}" class="component--contact-content">
  <p class="contact">Enquire.</p>
  @foreach ( $contacts as $contact )

    @php
      $contact_is_primary = isset( $contact->is_primary ) && $contact->is_primary ? true : false;
      $contact_email = isset( $contact->email ) ? $contact->email : '';
      $contact_name = isset( $contact->name ) ? $contact->name : '';
      $contact_phone = isset( $contact->phone ) ? $contact->phone : '';
    @endphp

    @if ( $contact_name && ( $contact_is_primary || $all ) )
      <p class="contact">
        <span class="contact-name">{!! $contact_name !!}</span><br>
        @if ( $contact_phone )
          <a class="contact-phone" href="tel:{!! $contact_phone !!}">{!! $contact_phone !!}</a><br>
        @endif
        @if ( $contact_email )
          <a class="contact-email" href="mailto:{!! $contact_email !!}">{!! $contact_email !!}</a><br>
        @endif
      </p>
    @endif

  @endforeach
</div>
