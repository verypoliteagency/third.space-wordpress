@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-ctas.blade.php';
  $ctas = isset( $ctas ) ? $ctas : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $ctas )
  <div class="component--ctas-content">
    {!! App\render_ctas( $ctas ) !!}
  </div>
@endif


