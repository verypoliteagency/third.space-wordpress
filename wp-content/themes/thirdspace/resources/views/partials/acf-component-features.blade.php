@php

  #TODO look at this on the contact page, should be showing up but is not

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-features.blade.php';
  $brochure = isset( $brochure ) ? $brochure : [];
  $brochure_url = isset( $brochure->url ) ? $brochure->url : '';
  $contacts = isset( $contacts ) ? $contacts : [];
  $features = isset( $features ) ? $features : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}">

  @if( is_singular('building') )
    <div class="row">
      <div class="col-12 offset-md-1 col-md-10 offset-lg-2 col-lg-8 mb-5">
        <h2>Building Highlights<span class="period">.</span></h2>
      </div>
    </div>
    <div class="row">
  @endif

  @if( is_singular('building') )
    <div class="component--features-sidebar col-12 col-sm-6 offset-md-1 col-md-5 offset-lg-2 col-lg-4">
      @if( $contacts )
        <div class="features-sidebar--contact">
          @foreach( $contacts as $contact )

            @php
              $contact_email = isset( $contact->email ) ? $contact->email : '';
              $contact_name = isset( $contact->name ) ? $contact->name : '';
            @endphp

            @if ( $contact_email && $contact_name )
              <p><a class="btn-text btn" href="mailto:{!! $contact_email !!}">Contact {!! $contact_name !!}</a></p>
            @endif

          @endforeach
        </div>
      @endif
      @if( $brochure )
        <p><a class="btn btn-outline-secondary" href="{{ $brochure_url }}" download target="_blank">Download Brochure</a></p>
      @endif
    </div>
  @endif

  @if( is_singular('building') )
    <div class="col-12 col-sm-6">
  @endif

  @if ( $features )
    <div class="row">
      @foreach( $features as $feature )

        @php
          $feature_details = isset( $feature->detail_text ) ? $feature->detail_text : '';
          $feature_heading = isset( $feature->heading ) ? $feature->heading : '';
        @endphp

        <dl class="col-sm-6 no-gutters mb-5">
          {!! $feature_heading ? '<dt class="col-8"><h3>' . $feature_heading . '</h3></dt>' : '' !!}
          {!! $feature_details ? '<dd class="col-8">' . $feature_details . '</dd>' : '' !!}
        </dl>

      @endforeach
    </div>
  @endif

  @if( is_singular('building') )
    </div>
    </div>
  @endif

</div>
