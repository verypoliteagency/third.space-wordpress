@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-gallery.blade.php';
  $gallery = isset( $gallery ) ? $gallery : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $gallery, '$gallery' );
  }

@endphp

@if ( $gallery )
  <div data-partial="{!! $partial_name !!}" class="gallery js-gallery">
    @foreach ( $gallery as $image )

      @php
        $image_id = isset( $image->id ) ? $image->id : [];
      @endphp

      @if ( $image_id )
        <div class="gallery-img-ctn">
          {!! wp_get_attachment_image( $image_id, "full", "", [ "class" => "gallery-img", 'loading' => 'eager'] ) !!}
        </div>
      @endif

    @endforeach
  </div>
@endif
