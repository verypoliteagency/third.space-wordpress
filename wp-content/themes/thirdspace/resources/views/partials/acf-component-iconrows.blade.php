@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-iconrows.blade.php';
  $iconrows = isset( $iconrows ) ? $iconrows : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $iconrows, '$iconrows' );
  }

@endphp

@if ( $iconrows )
  <div data-partial="{!! $partial_name !!}"  class="component--iconrow-content">
    @foreach( $iconrows as $iconrow )

      @php

        $cta = isset( $iconrow->component_ctas ) ? $iconrow->component_ctas : [];
        $description = isset( $iconrow->description ) ? $iconrow->description : '';
        $image = isset( $iconrow->image ) ? $iconrow->image : [];
        $title = isset( $iconrow->title ) ? $iconrow->title : '';

      @endphp

      <div data-aos="fade-in" data-aos-delay="{{ $loop->iteration * 150 }}" class="component--iconrow-iconrow iconrow">

        @if ( $image )
          <div class="component--iconrow-image iconrow__image">
            {!! wp_get_attachment_image( $image->id, "full","", ["class" => "component--iconrow-img iconrow-img-top"] ) !!}
          </div>
        @endif

        <div class="component--iconrow-body iconrow-body iconrow__body">
          @if ( $title )
            <h3 class="component--iconrow-title iconrow-title iconrow__title">{!! App\wrap_periods( $title ) !!}</h3>
          @endif
          @if( $description )
            <div class="component--iconrow-text iconrow-text iconrow__text">{!! $description !!}</div>
          @endif
          @if( $cta )
            <div class="component--iconrow-ctas iconrow__ctas">
              {!! App\render_ctas( $cta ) !!}
            </div>
          @endif
        </div>

      </div>

    @endforeach
  </div>
@endif

