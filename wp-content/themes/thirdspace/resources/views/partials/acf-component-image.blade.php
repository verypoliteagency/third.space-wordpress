@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-image.blade.php';

  $image = isset( $image ) ? $image : [];
  $image_content = isset( $image->image_content ) ? $image->image_content : [];

  $image_full_bleed_mobile = isset( $settings->full_bleed_mobile ) && $settings->full_bleed_mobile ? 'full-bleed-mobile' : '';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}" data-aos="fade-in" data-aos-duration="550" class="component--image-ctn {!! $image_full_bleed_mobile !!}">
  @if ( $image_content )
    @foreach( $image_content as $image )

      {!! wp_get_attachment_image( $image->id, "full", "", [ "class" => "component--image-image image-" . $loop->iteration ] ) !!}

      @if ( is_single() )
        <figcaption class="component--image-caption">
          {!! wp_get_attachment_caption( $image->id ) !!}
        </figcaption>
      @endif

    @endforeach
  @endif
</div>
