@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-linkimage.blade.php';

  $link_image = isset( $link_image ) ? $link_image : [];
  $image = isset( $link_image->image ) ? $link_image->image : [];
  $link = isset( $link_image->link ) ? $link_image->link : [];
  $link_url = isset( $link->url ) ? $link->url : '';
  $link_target = isset( $link->target ) ? $link->target : '';
  $link_title = isset( $link->title ) ? $link->title : '';

  $link_image_full_bleed_mobile = isset( $settings->full_bleed_mobile ) && $settings->full_bleed_mobile ? 'full-bleed-mobile' : '';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}" data-aos="fade-in" data-aos-duration="550" class="component--image-ctn linkimage {!! $link_image_full_bleed_mobile !!}">

  @if( $image )
    @if( $link )
      <a class="link--image" href="{!! $link_url !!}" {!! $link_target ? ' target=' . $link_target : '' !!}>
        {!! wp_get_attachment_image( $image->id, "full", "", [ "class" => "component--image-image image-" . $loop->iteration ] )!!}
        <h3>{!! $link_title !!}</h3>
      </a>
    @else
      {!! wp_get_attachment_image( $image->id, "full", "", [ "class" => "component--image-image image-" . $loop->iteration ] )!!}
    @endif
  @endif

</div>
