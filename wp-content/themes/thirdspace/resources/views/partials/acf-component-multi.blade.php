@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-multi.blade.php';
  $content_rows = isset( $component->content_row ) ? $component->content_row : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $content_rows )
  @foreach( $content_rows as $row )

    @php
      $columns = isset( $row->content_column ) ? $row->content_column : [];
    @endphp

    <div class="row component--multi-row">
      @foreach( $columns as $column )

        @php
          $building_type = isset( $building_type ) ? $building_type : [];
          $column_detail = isset( $column->detail ) ? $column->detail : '';
          $column_layout = isset( $column->acf_fc_layout ) ? $column->acf_fc_layout : 'not-set';
          $column_text = isset( $column->text ) ? $column->text : '';
          $column_title = isset( $column->title ) ? $column->title : '';
          $column_definition = isset( $column->definition ) ? $column->definition : '';
          $summary_text = isset( $summary_text ) ? $summary_text : '';
          $size = isset( $size ) ? $size : '';
          $status_override = isset( $status_override ) ? $status_override : '';
        @endphp

        @switch( $column_layout )

          @case( "custom_detail" )
            @if ( $column_title || $column_definition )
              <dl class="col-12 col-sm-4 ">
                @if( $column_title )<dt><h3>{!! $column_title !!}</h3></dt>@endif
                @if( $column_title )<dd><p>{!! $column_definition !!}</p></dd>@endif
              </dl>
            @endif
            @break

          @case( "detail" )

            @switch( $column_detail )
              @case( 'summary_text' )
                @if ( $summary_text )<div class="col-md-10">{!! $summary_text !!}</div>@endif
                @break
              @case( 'building_type' )
                @if ( $building_type )
                  <dl class="col-12 col-sm-4 ">
                    <dt><h3>Type.</h3></dt>
                    <dd><p>
                      @foreach( $building_type as $type )
                        {!! $loop->iteration > 1 ? '<br>' : '' !!}
                        {!! $type->name !!}
                      @endforeach
                    </p></dd>
                  </dl>
                @endif
                @break
              @case( 'size' )
                @if ( $size )
                  <dl class="col-12 col-sm-4 ">
                    <dt><h3>Size.</h3></dt>
                    <dd><p>{!! $size !!} sqft.</p></dd>
                  </dl>
                @endif
                @break
              @case( 'status_override' )
                @if ( $status_override )
                  <dl class="col-12 col-sm-4 ">
                    <dt><h3>Availability.</h3></dt>
                    <dd><p>{!! $status_override !!}</p></dd>
                  </dl>
                @endif
                @break
            @endswitch
            @break

          @case( "text" )
            {!! $column_text !!}
            @break

        @endswitch

      @endforeach
    </div>

  @endforeach
@endif

