@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-news.blade.php';
  $query = new WP_Query( ['post_type' => 'post' ]);
  $title = isset( $component->component_news->title ) ? $component->component_news->title : '';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $query->have_posts() )

  <div class="row align-items-center pb-5">
    <div class="col-6 col-md-5 offset-md-1">
      <h2>{!! App\wrap_periods( $title ) !!}</h2>
    </div>
    <div class="col-6 col-md-5 text-right">
      <a href="/news/" class="btn-text">View All Stories</a>
    </div>
  </div>

  @include('partials.content-template-news')

@endif
