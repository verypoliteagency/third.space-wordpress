@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-popup.blade.php';
  $component_ctas = isset( $component->component_text->component_ctas ) ? $component->component_text->component_ctas : [];
  $component_id = isset( $component_id ) ? $component_id : 'component_' . uniqid();
  $component_popup_ctas = isset( $component->component_popup->component_ctas ) ? $component->component_popup->component_ctas : [];
  $component_popup_image = isset( $component->component_popup->image ) ? $component->component_popup->image : [];
  $component_popup_image_large = isset( $component->component_popup->image_large ) ? $component->component_popup->image_large : [];
  $component_popup_subtitle = isset( $component->component_popup->subtitle ) ? $component->component_popup->subtitle : '';
  $component_popup_text = isset( $component->component_popup->text ) ? $component->component_popup->text : '';
  $component_popup_title = isset( $component->component_popup->title ) ? $component->component_popup->title : '';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $component_popup_image )
  <div data-aos="fade-in" data-aos-duration="550" class="component--popup-image-ctn">
    {!! wp_get_attachment_image( $component_popup_image->id, "full","", [ "class" => "component--popup-image js-popup-image" ] ) !!}
  </div>
@endif

@if ( $component_popup_image_large )
  <div class="d-none">
    {!! wp_get_attachment_image( $component_popup_image_large->id, "full", "", [ "class" => "component--popup-image js-popup-image-large", 'loading' => 'eager' ] )!!}
  </div>
@endif


<div class="component--popup-content">

  @if ( $component_popup_title )
    <h4 class="component--popup-title">{!! $component_popup_title !!}</h4>
  @endif

  @if ( $component_popup_subtitle )
    <h5 class="component--popup-subtitle">{!! $component_popup_subtitle !!}</h5>
  @endif

  <div class="component--popup-detail d-none">
    @if ( $component_popup_text )
      <div class="component--popup-text">
        {!! $component_popup_text !!}
      </div>
    @endif
    @if ( $component_popup_ctas )
      {!! App\render_ctas( $component_popup_ctas ) !!}
    @endif
  </div>

</div>

<button
  class="component--popup-btn"
  data-popup-target="#{{ $component_id }}"
  aria-label="More information about {!! $component_popup_title !!}"
></button>
