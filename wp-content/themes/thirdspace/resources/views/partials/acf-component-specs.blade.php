@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-specs.blade.php';
  $features = isset( $component->features ) ? $component->features : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if( $features )
  <div class="row">
    @foreach( $features as $feature )

      @php
        $feature_heading = isset( $feature->heading ) ? $feature->heading : '';
        $feature_details = isset( $feature->detail_text ) ? $feature->detail_text : '';
      @endphp

      <dl class="col-6">
        {!! $feature_heading ? '<dt><h3>' . $feature_heading . '</h3></dt>' : '' !!}
        {!! $feature_details ? '<dd><p>' . $feature_details . '</p></dd>' : '' !!}
      </dl>

    @endforeach
  </div>
@endif
