@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-component-text.blade.php';
  $component = isset( $component ) ? $component : false;
  $component_text_body = isset( $component->component_text->body ) ? $component->component_text->body : '';
  $component_ctas = isset( $component->component_text->component_ctas ) ? $component->component_text->component_ctas : false;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-aos="fade-in" class="component--text-content">
  @if ( $component_text_body )
    {!! App\text_wrap_periods( $component_text_body ) !!}
  @endif
  @if ( $component_ctas )
    {!! App\render_ctas( $component_ctas ) !!}
  @endif
</div>
