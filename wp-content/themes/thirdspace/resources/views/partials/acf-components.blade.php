@php

  // ---------------------------------------- Inherited
  // [ 'component_styles' => $component_styles, 'components' => $components ]

  // ---------------------------------------- Vars
  $partial_name = 'acf-components.blade.php';
  $components = isset( $components ) ? $components : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $components )
  @foreach ( $components as $component )

    @php

      $component_id = 'component_' . uniqid();
      $component_bleed_settings = isset( $component->layout_settings ) ? App\get_bleed_settings( $component->layout_settings ) : '';
      $component_cards = isset( $component->component_cards ) ? $component->component_cards : [];
      $component_classnames = isset( $component->layout_settings ) ? App\get_settings_classname( $component->layout_settings ) : '';
      $component_ctas = isset( $component->component_ctas ) ? $component->component_ctas : [];
      $component_features = isset( $component->features ) ? $component->features : [];
      $component_gallery = isset( $component->gallery ) ? $component->gallery : [];
      $component_iconrow = isset( $component->component_iconrow ) ? $component->component_iconrow : [];
      $component_image = isset( $component->component_image ) ? $component->component_image : [];
      $component_linkimage = isset( $component->component_linkimage ) ? $component->component_linkimage : [];
      $component_settings = isset( $component->layout_settings ) ? $component->layout_settings : [];
      $component_styles = isset( $component->layout_settings ) ? App\get_settings_styles( $component->layout_settings, 100, $component_id ) : '';
      $component_type = isset( $component->acf_fc_layout ) ? $component->acf_fc_layout : 'not-set';

      // ---------------------------------------- Debugging
      if ( $debugger_enabled && 'features' == $component_type && false ) {
        App\debug_this( $component_features, '$component_features' );
      }

    @endphp

    @if ( $component_styles )
      <style>{!! $component_styles !!}</style>
    @endif

    <div data-partial="{!! $partial_name !!}" id="{!! $component_id !!}" class="component component--{!! $component_type !!} col {!! $component_classnames !!} {!! $component_bleed_settings !!} {!! $component_type == "gallery" ? ' full-bleed full-bleed-mobile' : '' !!}">

      <div class="component--bkg"></div>

      @switch( $component_type )

        @case( "banner" )
          @include( 'partials.acf-component-banner', [ 'heading_tag' => 'h2' ] )
          @break

        @case( "cards" )
          @include( 'partials.acf-component-cards', [ 'cards' => $component_cards ] )
          @break

        @case( "contact" )
          @include( 'partials.acf-component-contact' )
          @break

        @case( "ctas" )
          @include( 'partials.acf-component-ctas', [ 'ctas' => $component_ctas ] )
          @break

        @case( "features" )
          @include( 'partials.acf-component-features', [ 'features' => $component_features ] )
          @break

        @case( "gallery" )
          @include( 'partials.acf-component-gallery', [ 'gallery' => $component_gallery ] )
          @break

        @case( "iconrows" )
          @include( 'partials.acf-component-iconrows', [ 'iconrows' => $component_iconrow ] )
          @break

        @case( "image" )
          @include( 'partials.acf-component-image', [ 'image' => $component_image, 'settings' => $component_settings ] )
          @break

        @case( "linkimage" )
          @include( 'partials.acf-component-linkimage', [ 'link_image' => $component_linkimage, 'settings' => $component_settings ] )
          @break

        @case( "multi" )
          @include('partials.acf-component-multi')
          @break

        @case( "news" )
          @include('partials.acf-component-news')
          @break

        @case( "popup" )
          @include( 'partials.acf-component-popup', [ 'component_id' => $component_id ] )
          @break

        @case( "text" )
          @include( 'partials.acf-component-text' )
          @break

      @endswitch

    </div>

  @endforeach
@endif
