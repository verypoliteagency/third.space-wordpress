@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-content-rows.blade.php';
  $content_rows = isset( $content_rows ) ? $content_rows : [];

  // LEGACY/NOT SURE
  // $GLOBALS['componentIndex'] = 1;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $content_rows )
  @foreach( $content_rows as $row )

    @php

      $row_classnames = '';
      $row_classnames .= isset( $row->no_gutters ) && $row->no_gutters ? ' no-gutters' : '';
      $row_classnames .= isset( $row->full_bleed ) && $row->full_bleed ? ' full-bleed' : '';
      $row_classnames .= isset( $row->row_reverse ) && $row->row_reverse ? ' flex-row-reverse' : '';
      $row_classnames .= isset( $row->align_items_center ) && $row->align_items_center ? ' align-items-center' : '';
      $row_classnames .= isset( $row->align_items_end ) && $row->align_items_end ? ' align-items-end' : '';
      $row_classnames .= isset( $row->no_padding ) && $row->no_padding ? ' pt-0 pb-0' : '';
      $row_components = isset( $row->components ) ? $row->components : [];
      $row_id = 'contentRow_' . uniqid();

    @endphp

    <div id ="{!! $row_id !!}" class="content-row row {{ trim( $row_classnames ) }}">
      @include( 'partials.acf-components', [ 'components' => $row_components ] )
    </div>

  @endforeach
@endif
