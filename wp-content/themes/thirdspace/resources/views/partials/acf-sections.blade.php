@php

  // ---------------------------------------- Vars
  $partial_name = 'acf-sections.blade.php';
  $sections = isset( $sections ) ? $sections : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $sections )
  @foreach ( $sections as $section )

    @php

      // ---------------------------------------- Vars
      $section_content_rows = isset( $section->content_rows ) ? $section->content_rows : [];
      $section_label = isset( $section->section_label ) ? $section->section_label : 'not-set';
      $section_label_classnames = isset( $section->label_advanced->label_settings ) ? App\get_settings_classname( $section->label_advanced->label_settings ) : '';
      $section_label_show = isset( $section->show_label_as_title ) && $section->show_label_as_title ? true : false;
      $section_classnames = isset( $section->layout_settings ) ? App\get_settings_classname( $section->layout_settings ) : '';
      $section_full_bleed = isset( $section->full_bleed ) && $section->full_bleed ? true : false;
      $section_id = preg_replace( '/[^a-z0-9]/i', '', $section_label );
      $section_no_gutters = isset( $section->no_gutters ) && $section->no_gutters ? true : false;
      $section_styles = isset( $section->layout_settings ) ? App\get_settings_styles( $section->layout_settings, 92, $section_id ) : '';
      $section_theme = isset( $section->theme ) ? $section->theme : 'not-set';

      // this might be buggy
      $section_full_bleed = isset( $section->full_bleed ) && $section->full_bleed ? 'full-bleed' : '';
      $section_no_gutters = isset( $section->no_gutters ) && $section->no_gutters ? 'no-gutters' : '';

    @endphp

    @if ( $section_styles )
      <style>{!! $section_styles !!}</style>
    @endif

    <section data-partial="{!! $partial_name !!}" id="{!! $section_id !!}" aria-label="{!! $section_label !!}" class="section container-fluid theme-{!! $section_theme !!} {!! $section_full_bleed !!} {!! $section_no_gutters !!}">

      <div class="section--bkg col-12 {{ $section_classnames }}"></div>

      <div class="section--content">
        @if ( $section_label_show )
          <div class="section--title-ctn row">
            <h2 class="section--title col {{ $section_label_classnames }}">{!! App\wrap_periods( $section_label ) !!}</h2>
          </div>
        @endif
        <div class="{!! $section_no_gutters !!}">
          @include( 'partials.acf-content-rows', [ 'content_rows' => $section_content_rows ] )
        </div>
      </div>

    </section>

  @endforeach
@endif

{{--

@if (!empty($sections))
  @foreach ($sections as $section)
      @php
      // App\console_log($section, 'section');
      // Section Header
      $showLabel = $section->show_label_as_title;
      $label = $section->section_label;
      $id = preg_replace ('/[^a-z0-9]/i', '', $label);
      $noSectionGutters = $section->no_gutters;
      $fullBleed = $section->full_bleed;



      // Get additional styles -- padding and margin -- using the function in helpers.php
      $sectionStyles = App\get_settings_styles($section->layout_settings, 92, $id);
      $sectionClassnames = App\get_settings_classname($section->layout_settings);
      $sectionLabelClassnames = App\get_settings_classname($section->label_advanced->label_settings);


    @endphp


    <style>{!! $sectionStyles !!}</style>
    <section
      id="{!! $id !!}"
      aria-label="{!! $label !!}"
      class="section container-fluid theme-{!! $section->theme !!} {!! ($fullBleed ? "full-bleed" : "") !!} {!! ($noSectionGutters ? "no-gutters" : "") !!}"
    >

      <div class="section--bkg col-12 {{$sectionClassnames}}"></div>

      <div class="section--content">
        {!! $showLabel ? '<div class="section--title-ctn row"><h2 class="section--title col ' . $sectionLabelClassnames . '">' . App\wrap_periods($label) . '</h2></div>'  : '' !!}

        <div class="{!! ($noSectionGutters ? "no-gutters" : "") !!}">
          @include('partials.acf-content-rows')
        </div>
      </div>
    </section>
  @endforeach
@endif




 --}}
