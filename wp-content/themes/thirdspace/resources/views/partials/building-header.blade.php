@php

  // ---------------------------------------- Vars
  $partial_name = 'building-header.blade.php';
  $splash_image = isset( $splash_image ) ? $splash_image : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}" class="page-header">
  <section id="hero" class="section container-fluid">

    <div class="section--bkg col-12"></div>

    <div class="component--banner theme-dark">
      <div class="component--banner-header">
        @if( $splash_image )
          <div class="component--banner-image-ctn">
            {!! wp_get_attachment_image( $splash_image->id, "full","", ["class" => "component--banner-image"] ) !!}
          </div>
        @endif
        <div class="row">
          <h1 class="component--banner-title col offset-lg-1 col-lg-6">{!! get_the_title() !!}</h1>
        </div>
      </div>
      <div class="component--banner-content">
        <div class="row">
          <div class="component--banner-header-text col-12 col-md-2 offset-lg-1 col-lg-6 col-xl-7">@php the_content() @endphp</div>
          <div class="component--banner-header-text col-12 col-md-10 offset-lg-1 col-lg-3 col-xl-3 text-right"><a href="#availability" class="btn btn-secondary">Check Availability</a></div>
        </div>
      </div>
    </div>

  </section>
</div>

