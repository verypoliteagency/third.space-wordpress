@php

  // ---------------------------------------- Vars
  $partial_name = 'building-list.blade.php';
  $buildings = get_pages([
    'sort_column' => 'menu_order',
    'sort_order' => 'ASC',
    'hierarchical' => 0,
    'post_type' => 'building',
  ]);

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $buildings )
  <section class="section section--our-spaces container-fluid container-limit">
    <div class="row">
      <div class="col-12">
        <div class="card-columns">
          @foreach( $buildings as $building )

            @php

              $building_id = isset( $building->ID ) ? $building->ID : false;
              $building_data = get_fields( $building_id ) ? get_fields( $building_id ) : [];
              $building_data_city = isset( $building_data['city'] ) ? $building_data['city'] : '';
              $building_data_street_address = isset( $building_data['street_address'] ) ? $building_data['street_address'] : '';
              $building_data_thumbnail_image_ID = isset( $building_data['thumbnail_image']['ID'] ) ? $building_data['thumbnail_image']['ID'] : false;
              $building_types = isset( $building_data['building_type'] ) ? $building_data['building_type'] : [];
              $building_type_attr = 'data-listing-type="';
              $building_legend = '';
              $building_post_title = isset( $building->post_title ) ? $building->post_title : '';

              foreach( $building_types as $building_type ) {
                $building_type_attr .= isset( $building_type->slug ) ? $building_type->slug . ' ' : '';
                $building_legend .= isset( $building_type->slug ) ? '<span class="property--legend-type" data-property-legend="' . $building_type->slug . '"></span>' : '';
              }

              $building_type_attr .= '"';

            @endphp

            <div class="card our-spaces--building listing" {!! $building_type_attr !!}>
              <a class="our-spaces--link" href="{!! get_permalink( $building_id ) !!}">
                @if ( $building_data_thumbnail_image_ID )
                  <div data-aos="fade-in" data-aos-duration="550" class="our-spaces--thumbnail-ctn">
                    {!! wp_get_attachment_image( $building_data_thumbnail_image_ID, "large", "", [ "class" => "card-img-top our-spaces--thumbnail" ] ) !!}
                  </div>
                @endif
                <div data-aos="fade-in" class="card-body our-spaces--details">
                  <div class="property--legend our-spaces--legend look-here">{!! $building_legend !!}</div>
                  @if ( $building_post_title )
                    <h5 class="card-title our-spaces--title">{!! $building_post_title !!}</h5>
                  @endif
                  @if ( ( $building_data_street_address || $building_data_city ) && false )
                    <div class="card-text our-spaces--address">
                      {!! $building_data_street_address !!} {!! $building_data_city !!}
                    </div>
                  @endif
                </div>
              </a>
            </div>

          @endforeach
        </div>
      </div>
    </div>
  </section>
@endif
