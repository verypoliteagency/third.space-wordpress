<section id="contactForm" class="section section--careers container-fluid bkg-lightgrey">
  <div class="section--title-ctn row">
    <h2 class="section--title col col-lg-10 offset-lg-1 pb-5">Current Openings<span class="period">.</span></h2>
  </div>
  <div class="row">
    <div class="component component--image col col-md-2 col-lg-2 col-xl-2 bleed-left d-none d-md-block ">
      <div class="component--image-ctn ">
        @include('svgs.icon-square')
      </div>
    </div>
    <div class="col-12 col-md-9 offset-md-1 col-lg-8 offset-lg-1 pt-5 pb-5">
      {!! do_shortcode('[awsmjobs]') !!}
    </div>
  </div>
</section>
