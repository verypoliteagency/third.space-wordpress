@php

  // ---------------------------------------- Vars
  $partial_name = 'contact-form.blade.php';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<section data-partial="{!! $partial_name !!}" id="contactForm" class="section section--form container-fluid bkg-lightgrey">
  <div class="section--title-ctn row">
    <h2 class="section--title col col-lg-10 offset-lg-1 pb-5">General Enquiries<span class="period">.</span></h2>
  </div>
  <div class="row">
    <div class="component component--image col col-md-2 col-lg-2 col-xl-2 bleed-left d-none d-md-block pt-3">
      <div class="component--image-ctn ">@include('svgs.icon-circle')</div>
    </div>
    <div class="col-12 col-md-10 col-lg-8 offset-lg-1 pt-5">
      @php gravity_form( 2, false, false, false, '', true ); @endphp
    </div>
  </div>
</section>
