@php

  // ---------------------------------------- Vars
  $partial_name = 'content-building.blade.php';
  $building_city = isset( $city ) ? $city : '';
  $building_blocks = isset( $building_blocks ) ? $building_blocks : [];
  $building_street_address = isset( $street_address ) ? $street_address : '';
  $formatted_address = $building_street_address . ', ' . $building_city . ', BC Canada';
  $formatted_address_uri = str_replace( ' ', '+', $formatted_address );
  $current_url = isset( $_SERVER['HTTP_REFERER'] ) ? parse_url( $_SERVER['HTTP_REFERER'] ) : '';
  $current_path = isset( $current_url['path'] ) ? $current_url['path'] : '';
  $related_buildings = isset( $related_buildings ) ? $related_buildings : [];
  $spaces = get_pages([
    'sort_column' => 'menu_order',
    'sort_order' => 'ASC',
    'hierarchical' => 0,
    'parent' => $post->ID,
    'post_type' => 'space',
  ]);

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    // App\debug_this( $building_street_address, '$building_street_address' );
  }

@endphp

<div class="page-content">


  <div class="component--breadcrumb container-fluid">
    <div class="row">
      <nav aria-label="breadcrumb" class=" col-12 offset-md-1 col-md-10 offset-lg-2 col-lg-8">
        <ol class="breadcrumb">
          @if( ('/our-spaces/' || '/find-a-space/') == $current_path )
            <li class="breadcrumb-item active" aria-current="page"><a class="btn btn-text" href="{!! $current_path !!}">Back to Spaces</a></li>
          @endif
        </ol>
      </nav>
    </div>
  </div>


  <section class="section section--building container-fluid">
    <div class="row">
      <div class="section--building-address col-12 offset-md-1 col-md-10 offset-lg-2 col-lg-8">
        @if( $building_street_address )
          <h2>{!! $building_street_address !!}{!! $building_city ? '<br>' . $building_city . '<span class="period">.</span>' : '' !!}</h2>
        @endif
      </div>
    </div>
    <div class="row">
      @if( $building_blocks )
        @include( 'partials.acf-components', [ 'components' => $building_blocks ] )
      @endif
    </div>
  </section>


  <section id="availability" class="section section--building-availablilty bkg-lightgrey container-fluid">
    <div class="row">
      <div class="col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-2">
        <h2>Availability<span class="period">.</span></h2>
      </div>
    </div>
    <div class="row">
      @if ( $spaces )

        <div class="space-listing--ctn col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-2">
          <div class="row">

            @foreach( $spaces as $space )

              @php

                $space_data = get_fields( $space->ID ) ? get_fields( $space->ID ) : [];
                $space_data_date = isset( $space_data['date'] ) ? $space_data['date'] : '';
                $space_data_lease_term = isset( $space_data['lease_term'] ) ? $space_data['lease_term'] : '';
                $space_data_price = isset( $space_data['price'] ) ? $space_data['price'] : '';
                $space_data_size = isset( $space_data['size'] ) ? $space_data['size'] : '';
                $space_data_space_type = isset( $space_data['space_type']->name ) ? $space_data['space_type']->name : '';
                $space_post_title = isset( $space->post_title ) ? $space->post_title : '';
                $space_url = get_permalink( $space->ID ) ? get_permalink( $space->ID ) : '';

              @endphp

              <div class="space-listing col-12 col-sm-6">
                <button class="space-listing--link" data-modal-url="{{ $space_url }}">
                  @if( $space_post_title )
                    <h3 class="space-listing--title">#{!! $space_post_title !!}</h3>
                  @endif
                  @if( $space_data_date )
                    <h5 class="space-listing--date">Available {!! $space_data_date !!}</h5>
                  @endif
                  @if( $space_data_space_type )
                    <h4 class="space-listing--type">{!! $space_data_space_type !!}</h4>
                  @endif
                  <div class="space-listing--details">
                    @if( $space_data_price )
                      <span class="space-listing--detail space-listing--price">${!! number_format( $space_data_price ) !!}/month</span>
                    @endif
                    @if( $space_data_lease_term )
                      <span class="space-listing--detail space-listing--term">{!! $space_data_lease_term !!} Term</span>
                    @endif
                    @if( $space_data_size )
                      <span class="space-listing--detail space-listing--price">{!! $space_data_size !!}sqft</span>
                    @endif
                  </div>
                </button>
              </div>

            @endforeach

          </div>
        </div>

      @else

        <div class="col-12 col-md-5 offset-md-1 col-lg-5 offset-lg-2 pt-5">
          <p>No available units. Please register to be notified when we have availability.</p>
        </div>

      @endif
    </div>
  </section>


  <section class="section section--building-location container-fluid">
    <div class="grid--location row">
      <div class="component component--location-map grid--location-map no-gutters">
        <a href="https://www.google.com/maps/place/{!! $formatted_address_uri !!}/" target="_blank">
          <img class="location--map-image" src="https://maps.googleapis.com/maps/api/staticmap?center={!! $formatted_address_uri !!}&zoom=13&scale=2&size=600x266&maptype=roadmap&key=AIzaSyDv02qE8sbKBORU4VDrUHRY0vWR_LlX0OU&map_id=3a6723300446bd8d&format=png32&visual_refresh=true&markers=size:mid%7Ccolor:0x00bdf2%7C{!! $formatted_address_uri !!}" alt="Map of {!! $formatted_address !!}" />
        </a>
      </div>
      <div class="component component--location-notify grid--location-notify form-mini full-bleed-mobile bkg-charcoal theme-dark">
        <p>Get notified when space becomes available<span class="period">.</span></p>
        @php
          add_filter( 'gform_field_value_building_name', function( $value ) {
            $buildingInfo = get_the_title();
            return $buildingInfo;
          });
          gravity_form( 3, false, false, false, '', true );
        @endphp
      </div>
      <div class="component component--location-address grid--location-address form-mini bkg-lightgrey theme-light">
        <p>
          <a href="https://www.google.com/maps/place/{!! $formatted_address_uri !!}/" target="_blank">
            @include('svgs.icon-map-marker')
            {!! $building_street_address !!}<br>
            {!! $building_city !!}, BC<br>
            {!! $postal_code !!}
          </a>
        </p>
      </div>
    </div>
  </section>

</div>

@if( $related_buildings )

  @php

    $buildings = get_pages([
      'sort_column' => 'menu_order',
      'sort_order' => 'ASC',
      'include' => $related_buildings,
      'hierarchical' => 0,
      'post_type' => 'building',
    ]);

  @endphp

  @if ( $buildings )

    <section class="section section--building-related container-fluid">

      <div class="row pb-5 align-items-baseline">
        <div class="col-12 col-md-8 offset-lg-1 col-lg-7">
          <h2>Similar Properties<span class="period">.</span></h2>
        </div>
        <div class="col-12 col-md-4 col-lg-3 text-right">
          <a class="btn btn-text mr-4" href="/our-spaces/">View All Spaces</a>
        </div>
      </div>

      <div class="row">
        <div class="col-12 offset-lg-1 col-lg-10">
          <div class="card-deck building-related--ctn">

            @foreach( $buildings as $building )

              @php


                $building_data = get_fields($building->ID ) ? get_fields($building->ID ) : [];
                $building_data_thumbnail_image = isset( $building_data['thumbnail_image'] ) ? $building_data['thumbnail_image'] : [];
                $building_data_city = isset( $building_data['city'] ) ? $building_data['city'] : '';
                $building_data_street_address = isset( $building_data['street_address'] ) ? $building_data['street_address'] : '';
                $building_data_type = isset( $building_data['building_type'] ) ? $building_data['building_type'] : [];
                $building_url = get_permalink( $building->ID ) ? get_permalink( $building->ID ) : '';
                $building_post_title = isset( $building->post_title ) ? $building->post_title : '';

                $building_type_attr = 'data-listing-type="';
                $building_legend = '';
                foreach( $building_data_type as $type ) {
                  $building_type_attr .= isset( $type->slug ) ? $type->slug . ' ' : '';
                  $building_legend .= isset( $type->slug ) ? '<span class="property--legend-type" data-property-legend="' . $type->slug . '"></span>' : '';
                }
                $building_type_attr .= '"';


              @endphp

              <div class="card building-related--listing listing" {!! $building_type_attr !!}>
                <a class="building-related--link" href="{!! $building_url !!}">
                  <div class="building-related--thumbnail-ctn">
                    {!! wp_get_attachment_image( $building_data_thumbnail_image['ID'], "large", "", [ "class" => "card-img-top building-related--thumbnail" ] ) !!}
                    <div class="property--legend building-related--legend">{!! $building_legend !!}</div>
                  </div>
                  <div class="card-body building-related--details">
                    <h4 class="card-title building-related--title">{!! $building_post_title !!}</h4>
                    @if( $building_data_street_address || $building_data_city )
                      <div class="card-text building-related--address">
                        {!! $building_data_street_address !!} {!! $building_data_city !!}
                      </div>
                    @endif
                  </div>
                </a>
              </div>

            @endforeach

          </div>
        </div>
      </div>

    </section>

  @endif
@endif
