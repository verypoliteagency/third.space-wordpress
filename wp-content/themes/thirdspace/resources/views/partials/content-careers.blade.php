@php

  // ---------------------------------------- Vars
  $partial_name = 'content-careers.blade.php';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<article data-partial="{!! $partial_name !!}" @php post_class() @endphp>

  <section class="section section--form container-fluid">
    <div class="row">
      <div class="col-12 col-sm-10 offset-sm-1 mb-5 pb-3">
        <h1>{!! the_title() !!}</h1>
      </div>
    </div>

    {!! the_content() !!}

    <div class="row align-items-end">
      <div class="col-6 col-md-4 offset-md-2 col-lg-4">
        <h2 class="heading-large">Share<span class="period">.</span></h2>
      </div>
      <div class="col-6 col-md-4">
        <div class="btn-share--group">
          <a class="btn-share" href="mailto:?subject=Shared%20article%20from%20Third.space&body={{ get_permalink() }}">
            <span class="material-icons">email</span>
          </a>
          <a class="btn-share twitter-share-button"
            href="https://twitter.com/intent/tweet?text=Read%20this%20article%20from%20Thirdspace:%20{{ get_permalink() }}"
            target="_blank">
            <span class="screen-reader-text">twitter</span>
            <span class="zoom-social_icons-list-span social-icon genericon genericon-twitter" data-hover-rule="background-color" data-hover-color="#  primary: #00bdf2," style="background-color : #414042; font-size: 15px; padding:3px; color: white" data-old-color="rgb(65, 64, 66)"></span>
          </a>
          <a class="btn-share" href="https://www.facebook.com/sharer.php?u={{ get_permalink() }}" target="_blank">
            <span class="screen-reader-text">facebook</span>
            <span class="zoom-social_icons-list-span social-icon genericon genericon-facebook" data-hover-rule="background-color" data-hover-color="#00bdf2" style="background-color: rgb(65, 64, 66); font-size: 15px; padding: 3px;color:white;" data-old-color="rgb(65, 64, 66)"></span>
          </a>
        </div>
      </div>
    </div>
  </section>

</article>
