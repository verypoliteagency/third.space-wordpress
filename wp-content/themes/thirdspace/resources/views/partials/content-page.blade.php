@php

  // ---------------------------------------- Vars
  $partial_name = 'content-page.blade.php';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}" class="page-content">

  @include( 'partials.acf-sections' )

  @if ( is_page('contact') )
    @include( 'partials.contact-form' )
  @endif

  @if ( is_page('find-a-space') )
    @include( 'partials.space-list' )
  @endif

  @if ( is_page('our-spaces') )
    @include( 'partials.building-list' )
  @endif

  @if ( is_singular('space') )
    @include( 'partials.space-list' )
  @endif

</div>
