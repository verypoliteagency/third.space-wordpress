@php

  // ---------------------------------------- Vars
  $partial_name = 'content-single.blade.php';
  $image_id = get_post_thumbnail_id() ? get_post_thumbnail_id() : [];
  $related_posts = isset( $related_posts ) ? $related_posts : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div data-partial="{!! $partial_name !!}" class="page-header">
  <section id="hero" class="section container-fluid">
    <div class="section--bkg col-12">
    </div>
    <div class="component--banner theme-dark">
      <div class="component--banner-header">
        @if ( $image_id )
          <div class="component--banner-image-ctn">
            {!! wp_get_attachment_image( $image_id, "full","", ["class" => "component--banner-image"] ) !!}
          </div>
        @endif
        <div class="row">
          <h1 class="component--banner-title col offset-md-1 col-md-8 col-lg-6">{!! get_the_title() !!}</h1>
        </div>
      </div>
      @if ( has_excerpt() )
        <div class="component--banner-content">
          <div class="row">
            <div class="component--banner-header-text col-12 offset-md-1 col-md-8 col-lg-6">@php the_excerpt() @endphp</div>
          </div>
        </div>
      @endif
    </div>
  </section>
</div>

<article @php post_class() @endphp>
  <section class="section section--news container-fluid">
    <div class="row">
      <div class="col-12 col-md-8 offset-md-2 col-lg-6 col-xl-6">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page"><a href="/news/" class="btn-text">All News</a></li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="row">
      <div class="component component--news col-12 col-md-8 offset-md-2 col-lg-6 col-xl-6    ">
        <div class="component--news-content">
          {!! the_content() !!}
        </div>
      </div>
    </div>
    <div class="row align-items-end">
      <div class="col-6 col-md-4 offset-md-2 col-lg-4">
        <h2 class="heading-large">Share<span class="period">.</span></h2>
      </div>
      <div class="col-6 col-md-4">
        <div class="btn-share--group">
          <a class="btn-share" href="mailto:?subject=Shared%20article%20from%20Third.space&body={{ get_permalink() }}">
            <span class="material-icons">email</span>
          </a>
          <a class="btn-share twitter-share-button"
            href="https://twitter.com/intent/tweet?text=Read%20this%20article%20from%20Thirdspace:%20{{ get_permalink() }}"
            target="_blank">
            <span class="screen-reader-text">twitter</span>
            <span class="zoom-social_icons-list-span social-icon genericon genericon-twitter" data-hover-rule="background-color" data-hover-color="#  primary: #00bdf2," style="background-color : #414042; font-size: 15px; padding:3px; color: white" data-old-color="rgb(65, 64, 66)"></span>
          </a>
          <a class="btn-share" href="https://www.facebook.com/sharer.php?u={{ get_permalink() }}" target="_blank">
            <span class="screen-reader-text">facebook</span>
            <span class="zoom-social_icons-list-span social-icon genericon genericon-facebook" data-hover-rule="background-color" data-hover-color="#00bdf2" style="background-color: rgb(65, 64, 66); font-size: 15px; padding: 3px;color:white;" data-old-color="rgb(65, 64, 66)"></span>
          </a>
        </div>
      </div>
    </div>
  </section>
</article>

@if ( $related_posts )
  <section class="section section--news-related bkg-lightgrey container-fluid">
    <div class="row align-items-center pb-5">
      <div class="col-6 col-md-5 offset-md-1">
        <h2>Related News<span class="period">.</span></h2>
      </div>
      <div class="col-6 col-md-5 text-right">
        <a href="/news/" class="btn-text">View All Stories</a>
      </div>
    </div>
    @include( 'partials.content-template-news' )
  </section>
@endif
