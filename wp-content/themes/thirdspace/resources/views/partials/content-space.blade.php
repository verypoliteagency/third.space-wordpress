@php

  // ---------------------------------------- Vars
  $partial_name = 'content-space.blade.php';
  $parent_post_id = wp_get_post_parent_id( $post ) ? wp_get_post_parent_id( $post ) : false;
  $parent_fields = get_fields( $parent_post_id ) ? get_fields( $parent_post_id ) : [];
  $parent_fields_city = isset( $parent_fields['city'] ) ? $parent_fields['city'] : [];
  $parent_fields_province = isset( $parent_fields['province'] ) ? $parent_fields['province'] : [];
  $parent_fields_street_address = isset( $parent_fields['street_address'] ) ? $parent_fields['street_address'] : [];
  $parent_contacts = isset( $parent_fields['contacts'] ) ? $parent_fields['contacts'] : [];
  // LEGACY // returns true if is_primary is NOT set to true anywhere in contacts
  $all = array_search( true, array_column( $parent_contacts, 'is_primary' ) ) === false;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<section data-partial="{!! $partial_name !!}" id="space" class="section section--space-header container-fluid">

  <div class="row">
    <div class="col-12">
      <h2>Space Details.</h2>
    </div>
  </div>

  <div class="row">
    <div class="space-header--text col-12 offset-md-1 col-md-8 offset-lg-3 col-lg-6">@php the_content() @endphp</div>
    <div class="space-header--contacts col-12 col-md-3 col-lg-2 offset-lg-1">
      @foreach ( $parent_contacts as $contact )

        @php
          $contact_name = isset( $contact['name'] ) ? $contact['name'] : '';
          $contact_email = isset( $contact['email'] ) ? $contact['email'] : '';
          $contact_is_primary = isset( $contact['is_primary'] ) && $contact['is_primary'] ? true : false;
        @endphp

        @if ( $contact_name && ( $contact_is_primary || $all ) )
          <a class="contact-email btn btn-text" href="mailto:{!! $contact_email !!}">Contact {!! $contact_name !!}</a>
        @endif

      @endforeach
    </div>
  </div>

</section>


<section class="section section--space-details container-fluid bkg-lightgrey">
  <div class="row">

    <div class="space-details--info col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 bleed-left">

      <h4 class="adr">
        <span class="street-address">#{!! App::title() !!} {!! $parent_fields_street_addres !!}</span><br>
        <span class="locality">{!! $parent_fields_city !!}</span> <span class="locality">{!! $parent_fields_province !!}</span>
      </h4>

      @php

        $date = isset( $date ) ? $date : '';
        $price = isset( $price ) ? $price : '';
        $size = isset( $size ) ? $size : '';
        $space_type = isset( $space_type ) ? $space_type : [];
        $space_type_name = isset( $space_type->name ) ? $space_type->name : '';
        $specs = isset( $specs ) ? $specs : [];
        $specs_url = isset( $specs->url ) ? $specs->url : '';
        $term = isset( $term ) ? $term : '';
        $under_development = isset( $under_development ) ? $under_development : '';

      @endphp

      <div class="specs">

        @if ( $space_type && $space_type_name )
          <dl class="row">
            <dt class="col-4">Space Type.</dt>
            <dd class="col-8">{!! $space_type_name !!}</dd>
          </dl>
        @endif

        @if ( $size )
          <dl class="row">
            <dt class="col-4">Size.</dt>
            <dd class="col-8">{!! number_format( $size ) !!} sqft</dd>
          </dl>
        @endif

        @if ( $price )
          <dl class="row">
            <dt class="col-4">Price.</dt>
            <dd class="col-8">{!! $price !!}</dd>
          </dl>
        @endif

        @if ( $term )
          <dl class="row">
            <dt class="col-4">Term.</dt>
            <dd class="col-8">{!! $term !!}</dd>
          </dl>
        @endif

        @if( $date )
          <dl class="row">
            <dt class="col-4">Available.</dt>
            <dd class="col-8">{!! $date !!}</dd>
          </dl>
        @endif

        @if( $under_development )
          <dl class="row">
            <dt class="col-4">Under Development.</dt>
            <dd class="col-8">{!! $under_development !!}</dd>
          </dl>
        @endif

        @if( $specs && $specs_url )
          <a class="btn btn-outline-secondary" href="{!! $specs_url !!}" download target="_blank">Download Specs</a>
        @endif

      </div>
    </div>

    <div class="component space-details--gallery col-12 col-sm-6 col-md-8 col-lg-9 col-xl-9 bleed-right full-bleed-mobile">
      @include( 'partials.acf-component-gallery' )
    </div>

  </div>
</section>
