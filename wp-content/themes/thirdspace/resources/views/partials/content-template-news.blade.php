@php

  // ---------------------------------------- Vars
  $partial_name = 'content-template-news.blade.php';
  $item_count = 0;
  $news_children = [];
  $related_posts = isset( $related_posts ) ? $related_posts : [];

  if ( is_single() && $related_posts ) {
    $news_children = PageTemplateNews::newsLoop( $related_posts );
  } elseif ( !is_page('News') ) {
    $item_count = isset( $component->component_news->news_count ) && !empty( $component->component_news->news_count ) ? $component->component_news->news_count : 3;
    $news_children = array_slice( PageTemplateNews::newsLoop(), 0, $item_count );
  } else {
    $news_children = PageTemplateNews::newsLoop();
  }

  // ---------------------------------------- Debugging
  if ( $debugger_enabled ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<div class="row">
  <div class="col-12 offset-lg-1 col-lg-10">
    <div class="card-deck news-listing--ctn">
      @if ( $news_children )
        @foreach ( $news_children as $news )

          @php
            $news_excerpt = isset( $news['excerpt'] ) ? $news['excerpt'] : '';
            $news_link = isset( $news['link'] ) ? $news['link'] : '';
            $news_thumbnail = isset( $news['thumbnail'] ) ? $news['thumbnail'] : [];
            $news_title = isset( $news['title'] ) ? $news['title'] : '';
          @endphp

          <div class="card news-listing">
            @if ( $news_thumbnail )
              <div data-aos="fade-in" data-aos-duration="550" class="news-listing--image-ctn">
                {!! wp_get_attachment_image( $news_thumbnail, "full", "", [ "class" => "news-listing--image card-img-top" ] ) !!}
              </div>
            @endif
            @if ( $news_title || $news_excerpt )
              <div data-aos="fade-in" class="card-body news-listing--details">
                @if ( $news_title )
                  <h3 class="card-title news-listing--title">{!! App\truncateToWord( $news_title, 40) !!}</h3>
                @endif
                @if ( $news_excerpt )
                  <p class="card-text news-listing--text">{!! App\truncateToWord( $news_excerpt, 100) !!}</p>
                @endif
                @if ( $news_link )
                  <a href="{!! $news_link !!}" class="card-link news-listing--link btn-text">Read More</a>
                @endif
              </div>
          </div>
        @endforeach
      @endif
    </div>
  </div>
</div>
