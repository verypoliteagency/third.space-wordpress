@php

  // ---------------------------------------- Vars
  $flexible_content = get_field( 'flex_content' ) ? get_field( 'flex_content' ) : [];

@endphp

@if( $flexible_content )
  <div class="flexible-content">
    @foreach ( $flexible_content as $content_row )

      @php
        $row_index = $loop->iteration;
        $row_layout = isset( $content_row['acf_fc_layout'] ) ? $content_row['acf_fc_layout'] : 'not-set';
        $row_classes = 'flexible-content__section section section';
        $row_classes .= ' section--' . $row_layout . ' ' . $row_layout;
        $row_colour_theme = isset( $content_row['colour_theme'] ) ? $content_row['colour_theme'] : 'not-set';
        if ( $debugger_enabled && false ) App\debug_this( $content_row, '$content_row' );
      @endphp

      <section class="{{ $row_classes }}" data-colour-theme="{{ $row_colour_theme }}">
        @switch( $row_layout )
          @case( 'cta-feature' )
             @include( 'flexible-sections.cta-feature', [ 'block_name' => $row_layout, 'container' => true, 'content' => $content_row, 'index' => $row_index ] )
            @break
          @case( 'image-feature' )
            @include( 'flexible-sections.image-feature', [ 'block_name' => $row_layout, 'container' => false, 'content' => $content_row, 'index' => $row_index ] )
            @break
          @case( 'text-feature' )
            @include( 'flexible-sections.text-feature', [ 'block_name' => $row_layout, 'container' => true, 'content' => $content_row, 'index' => $row_index ] )
            @break
        @endswitch
      </section>


    @endforeach
  </div>
@endif
