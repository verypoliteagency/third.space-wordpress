<footer class="content-info">
  <div class="container-fluid">
    <div class="row">
      <div class="content-info--col content-info--col-brand col-3 col-sm-2 col-lg-1">
        <a class="brand content-info--brand" href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">
          <div class="brand__svg">@include('svgs.logo-square')</div>
        </a>
      </div>
      <div class="content-info--col content-info--col-address col-12 col-sm-5 col-lg-7 col-xl-6 offset-xl-1">
        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
      <div class="content-info--col content-info--col-contact form-mini col-12 col-sm-5 col-md-5 col-lg-4 offset-xl-1 col-xl-3 bleed-right">
        @php dynamic_sidebar('sidebar-footer-contact') @endphp
      </div>
    </div>
  </div>
</footer>

@if ( $debugger_enabled )
  @include('partials.grid-guides')
@endif

@include('scripts.facebook-pixel')
