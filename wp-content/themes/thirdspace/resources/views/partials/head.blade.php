<head>

  <meta name="facebook-domain-verification" content="bgr8y2i48yb8zln6ccry8qwxvjay3r" />
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="192x192" href="/icons/android-chrome-192x192.png" sizes=255x255>
  <link rel="icon" type="image/png" sizes="512x512" href="/icons/android-chrome-512x512.png" sizes=256x256>
  <link rel="icon" href="/icons/favicon.ico" />
  <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-GGDBL8TMRF"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-196714021-1');
    gtag('config', 'G-GGDBL8TMRF');
  </script>

  @php wp_head() @endphp

</head>
