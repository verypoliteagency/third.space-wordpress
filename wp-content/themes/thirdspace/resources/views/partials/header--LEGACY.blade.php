<header class="banner no-gutters">
  <div class="container-fluid">

    <a class="brand banner--brand" href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">
      <div class="brand__svg">@include('svgs.logo-main')</div>
    </a>

    @if (has_nav_menu('primary_navigation'))
      <nav class="nav-quick	nav-pinned">
        {!! wp_nav_menu([
          'theme_location'    => 'primary_navigation',
          'menu_class'        => 'nav',
          'container'         => 'ul',
          'walker'            => new \App\wp_bootstrap4_navwalker()
        ]) !!}
      </nav>
    @endif

    <nav class="main-nav navbar navbar-expand-xs" aria-label="Main Menu">

      <div class="cta d-md-none">
        <a class="link" href='{{ home_url('/') }}services'><span class="link__title">Services</span></a>
      </div>

      <button
        class="navbar-toggler navbar-toggler--menu collapsed"
        type="button"
        data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
      >
        <div class="navbar-toggler-icon navbar-menu-icon"><span class="material-icons">menu</span></div>
        <div class="navbar-toggler-icon navbar-close-icon"><span class="material-icons">close</span></div>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-ctn">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu([
                'theme_location'  => 'primary_navigation',
                'menu_class'      => 'nav flex-column nav-primary',
                'container'       => 'ul',
                'walker'          => new \App\wp_bootstrap4_navwalker()
              ])
            !!}
          @endif
          @if (has_nav_menu('secondary_navigation'))
            {!! wp_nav_menu([
                'theme_location'  => 'secondary_navigation',
                'menu_class'      => 'nav flex-column nav-secondary',
                'container'       => 'ul',
                'walker'          => new \App\wp_bootstrap4_navwalker()
              ])
            !!}
          @endif
          @php dynamic_sidebar('sidebar-primary') @endphp
          <a class="brand nav--brand nav-link" href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">
            <div class="brand__svg">@include('svgs.logo-main')</div>
          </a>
        </div>
      </div>

    </nav>

    @if (has_nav_menu('optional_navigation'))
      <nav class="nav-option nav-pinned">
        {!! wp_nav_menu([
          'theme_location'    => 'optional_navigation',
          'menu_class'        => 'nav',
          'container'         => 'ul',
          'walker'            => new \App\wp_bootstrap4_navwalker()
        ]) !!}
      </nav>
    @endif

  </div>
</header>
