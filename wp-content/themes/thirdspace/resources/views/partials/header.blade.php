@php
  $current_post_id = get_the_ID();
@endphp

<header class="header sticky">
 {!! App\render_container( 'open', 'col-12', 'container-fluid' ) !!}
  <div class="header__main">

    <div class="header__brand brand primary">
      <a href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">@include('svgs.logo-main')</a>
    </div>

    @if ( has_nav_menu('primary_navigation') )
      <nav class="navigation main-menu d-none d-md-inline-flex">
        {!! App\render_wp_navigation( 'Main Navigation', $current_post_id ) !!}
      </nav>
    @endif

    <div class="extra-link d-none d-sm-block">
      <a class='link' href="{{ home_url('/') }}service" target="_self">Service</a>
    </div>

    <button
      class="navbar-toggler navbar-toggler--menu js--menu-trigger-button"
      type="button"
    >
      <div class="navbar-toggler-icon icon-menu">@include('svgs.icon-menu')</div>
      <div class="navbar-toggler-icon icon-close">@include('svgs.icon-close')</div>
    </button>

    <div class="header__collapse-navigation">
      <div class="header__collapse-navigation-header">
        <div class="container-fluid">
          <div class="brand mobile-menu d-sm-none">
            <a href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">@include('svgs.logo-main')</a>
          </div>
        </div>
      </div>
      <div class="header__collapse-navigation-body">
        @if ( has_nav_menu('primary_navigation') )
          <nav class="navigation mobile-menu primary">
            {!! App\render_wp_navigation( 'Main Navigation', $current_post_id ) !!}
          </nav>
        @endif
        @if ( has_nav_menu('secondary_navigation') )
          <nav class="navigation mobile-menu secondary">
            {!! App\render_wp_navigation( 'Full Navigation', $current_post_id ) !!}
          </nav>
        @endif
        @php dynamic_sidebar('sidebar-primary') @endphp
        <div class="brand mobile-menu d-none d-sm-block">
          <a href="{{ home_url('/') }}" aria-label="{{ get_bloginfo('name', 'display') }}">@include('svgs.logo-main')</a>
        </div>
      </div>
    </div>

  </div>
 {!! App\render_container( 'closed' ) !!}
</header>
