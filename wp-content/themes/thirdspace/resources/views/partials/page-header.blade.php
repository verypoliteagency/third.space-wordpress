@php

  // ---------------------------------------- Vars
  $partial_name = 'page-header.blade.php';
  $acf_options = isset( $acf_options ) ? $acf_options : false;
  $component_banner = isset( $component_banner ) ? $component_banner : [];
  $component_banner_bg_color = isset( $component_banner->background_color ) ? $component_banner->background_color : 'not-set';
  $component_banner_theme = isset( $component_banner->theme ) ? $component_banner->theme : 'not-set';
  $heading_tag = isset( $heading_tag ) ? $heading_tag : 'h2';
  $section_theme = isset( $section->theme ) ? $section->theme : 'not-set';

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    // App\debug_this( $acf_options, '$acf_options' );
    // App\debug_this( $component_banner, '$component_banner' );
  }

@endphp

<div data-partial="{!! $partial_name !!}" class="page-header">
  <section id="hero" class="section container-fluid theme-{!! $section_theme !!}">
    @if( !is_front_page() )
      <div class="section--bkg col-12 theme-{!! $component_banner_theme !!} bkg-{!! $component_banner_bg_color !!}"></div>
    @endif
    @include( 'partials.acf-component-banner', [ 'heading_tag' => 'h1' ] )
  </section>
</div>
