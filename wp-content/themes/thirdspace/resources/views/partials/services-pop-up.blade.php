@php

  // ---------------------------------------- Vars
  $partial_name = 'services-pop-up.blade.php';
  $services = $acf_options->services ?? [];
  $services_delay = $services->delay ?? 2500;
  $services_duration = $services->duration ?? 5500;
  $services_messages = $services->message ?? '';
  $services_enable = $services->enable ?? false;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
    App\debug_this( $services->message, '$services' );
  }

@endphp

@php if ( $services_enable ) : @endphp
  <div class="services-cta" id="services-cta" data-delay="{!! $services_delay !!}" data-duration="{!! $services_duration !!}">
    <div class="services-cta__circle"></div>
    <div class="services-cta__main">
      <div class="message">{!! $services_messages !!}</div>
    </div>
  </div>
@php endif; @endphp
