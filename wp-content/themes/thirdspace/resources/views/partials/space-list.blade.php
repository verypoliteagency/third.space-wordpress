@php

  // ---------------------------------------- Vars
  $partial_name = 'space-list.blade.php';
  $buildings = get_pages([
    'sort_column' => 'menu_order',
    'sort_order' => 'ASC',
    'hierarchical' => 0,
    'post_type' => 'building',
  ]);
  $available_count = 0;

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

<section class="section section--find-a-space container-fluid">
  <div class="row">

    <div class="col-12 col-sm-10 col-lg-10 offset-sm-1">
      @if ( App\check_available() )
        <h2>Available Spaces<span class="period">.</span></h2>
        <div class="property--filters">
          <span class="property--filters-btn" data-filter-type="residential">Residential</span>
          <span class="property--filters-btn" data-filter-type="commercial">Commercial</span>
          <span class="property--filters-btn" data-filter-type="under_development">Under Development</span>
        </div>
      @else
        <h2>No available spaces<span class="period">.</span><br><br><br></h2>
      @endif
    </div>

    <div class="col-12 col-sm-10 col-lg-10 offset-sm-1">
      @foreach( $buildings as $building )

        @php

          $building_id = isset( $building->ID ) ? $building->ID : false;
          $building_data = get_fields( $building_id ) ? get_fields( $building_id ) : [];
          $building_data_city = isset( $building_data['city'] ) ? $building_data['city'] : '';
          $building_data_splash_image_ID = isset( $building_data['splash_image']['ID'] ) ? $building_data['splash_image']['ID'] : false;
          $building_data_street_address = isset( $building_data['street_address'] ) ? $building_data['street_address'] : '';
          $building_data_summary_text = isset( $building_data['summary_text'] ) ? $building_data['summary_text'] : '';
          $building_post_title = isset( $building->post_title ) ? $building->post_title : '';
          $spaces = get_pages([
            'sort_column' => 'menu_order',
            'sort_order' => 'ASC',
            'hierarchical' => 0,
            'parent' => $building_id,
            'post_type' => 'space',
          ]);

        @endphp

        @if ( $spaces )

          @php

            $available = false;
            $space_type_attr = 'data-listing-type="';
            $space_legend = '';

            foreach ( $spaces as $space ) {
              $space_data = get_fields( $space->ID ) ? get_fields( $space->ID ) : [];
              $available = isset( $space_data['available'] ) && $space_data['available'] ? true : false;
              if ( $available ) {
                $space_type_attr .= isset( $space_data['space_type']->slug ) ? $space_data['space_type']->slug . ' ' : '';
                $space_legend .= isset( $space_data['space_type']->slug ) ? '<span class="property--legend-type" data-property-legend="' . $spaceData['space_type']->slug . '"></span>' : '';
              }
            }

            $space_type_attr .= '"';

          @endphp

          @if ( $available )
            @php $available_count ++; @endphp
            <div class="find-a-space--building listing" {!! $space_type_attr !!}>
              <a class="find-a-space--link-wrapper row {!! $available_count % 2 ? 'flex-row-reverse' : '' !!}" href="{!! get_permalink( $building_id ) !!}">

                @if ( $building_data_splash_image_ID )
                  <div data-aos="fade-in" data-aos-duration="550" class="find-a-space--thumbnail-ctn image-fade col">
                    {!!  wp_get_attachment_image( $building_data_splash_image_ID, "large", "", [ "class" => " find-a-space--thumbnail" ] ) !!}
                    <div class="property--legend find-a-space--legend">{!! $space_legend !!}</div>
                  </div>
                @endif

                <div data-aos="fade-in" class="find-a-space--details col">
                  <h3 class="find-a-space--title">{!! $building_post_title !!}</h3>
                  @if ( $building_data_street_address || $building_data_city )
                    <h4 class="find-a-space--address">{!! $building_data_street_address !!} {!! $building_data_city !!}</h4>
                  @endif
                  @if( $building_data_summary_text )
                    <div class="find-a-space--text">{!! $building_data_summary_text !!}</div>
                  @endif
                  <span class="find-a-space--link btn-text">View Space Details</span>
                </div>

              </a>
            </div>
          @endif

        @endif

      @endforeach
    </div>

  </div>
</section>
