@php

  // ---------------------------------------- Vars
  $partial_name = 'template-news-header.blade.php';
  $featured_post = isset( $featured_post ) ? $featured_post : [];

  // ---------------------------------------- Debugging
  if ( $debugger_enabled && false ) {
    echo '<h1>' . $partial_name . '</h1>';
  }

@endphp

@if ( $featured_post )

  @php

    // LEGACY GARBAGE
    // if(!empty($featured_post)) {
    //   $topPost = $featured_post;
    // } else {
    //   $topPost = get_posts( array(
    //     'post_type' => 'post',
    //     'numberposts' => 1,
    //   ))[0];
    // }
    // App\console_log($topPost, 'topPost');

    $featured_post_image_id = get_post_thumbnail_id( $featured_post ) ? get_post_thumbnail_id( $featured_post ) : false;
    $featured_post_id = isset( $featured_post->ID ) ? $featured_post->ID : false;
    $featured_post_excerpt = get_the_excerpt( $featured_post_id ) ? get_the_excerpt( $featured_post_id ) : '';
    $featured_post_title = isset( $featured_post->post_title ) ? $featured_post->post_title : false;

  @endphp

  <div class="page-header">
    <section id="hero" class="section section--news-feature container-fluid bkg-lightgrey">
      <div class="section--bkg col-12">
      </div>
      <div class="component--banner news-feature theme-light">
        <div class="component--banner-header">
          <div class="row">
            @if ( $featured_post_title )
              <h1 class="component--banner-title col offset-lg-1 col-lg-7">{!! $featured_post_title !!}</h1>
            @endif
          </div>
        </div>
        <div class="component--banner-content">
          <div class="row align-items-center">
            <div class="component--banner-header-text col col-9 offset-md-1 col-md-6 offset-lg-3 col-lg-5">
              @if ( $featured_post_excerpt )
                <p>{!! App\truncateToWord( $featured_post_excerpt, 100 ) !!}</p>
              @endif
              @if ( $featured_post_id )
                <a href="{!! the_permalink( $featured_post_id ) !!}" class="btn-text">Read More</a>
              @endif
            </div>
            @if ( $featured_post_image_id )
              <div class="component--image-image-ctn col col-12 col-md-5 col-lg-4 col-xl-4 bleed-right">
                <div data-aos="fade-in" data-aos-duration="550" class="news-feature--image-ctn">
                  {!! wp_get_attachment_image( $featured_post_image_id, "full", "", [ "class" => "component--image-image news-feature--image" ] ) !!}
                </div>
              </div>
            @endif
          </div>
        </div>
      </div>
    </section>
  </div>

@endif
