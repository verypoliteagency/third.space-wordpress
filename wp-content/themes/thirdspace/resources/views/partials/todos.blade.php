<div class="todos">
  <div class="container">
    <div class="row">
      <div class="col-12">

        <h2>Todos</h2>

        <ul>
          <li>VC app directory</li>
          <li>VC base views</li>
          <li>VC Partials</li>
        </ul>

        <h2>Done</h2>
        <ul>
          <li><strong>/app/</strong></li>
          <li>admin.php</li>
          <li>filters.php</li>
          <li>helpers.php</li>
          <li>setup.php</li>
          <li>shortcodes.php</li>
          <li><strong>/app/Controllers</strong></li>
          <li>App.php</li>
          <li>FrontPage.php</li>
          <li>PageTemplateNews.php</li>
        </ul>

        <div class="test-image">
          <img src="@asset('images/placeholder--vancouver-sample-image.jpeg')" />
        </div>

      </div>
    </div>
  </div>
</div>
