@extends('layouts.app')

@section('content')
  @while(have_posts())

    @php the_post() @endphp

    @include('partials.building-header')
    @include('partials.content-building')

  @endwhile
@endsection
