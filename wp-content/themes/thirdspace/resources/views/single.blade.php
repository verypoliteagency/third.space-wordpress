@extends('layouts.app')

@section('content')
  @while( have_posts() )

    @php the_post() @endphp

    @if ( !is_singular('awsm_job_openings') )
      @include( 'partials.content-single-' . get_post_type() )
    @else
      @include( 'partials.content-careers' )
    @endif

  @endwhile
@endsection
