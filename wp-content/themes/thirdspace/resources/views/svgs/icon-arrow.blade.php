<svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0_397_228)">
    <path d="M17.6397 7.46191H0.503906V9.54044H17.6397V7.46191Z" fill="white"/>
    <path d="M10.4287 0L8.94385 1.52194L16.8056 8.49885L8.94385 15.4781L10.4287 17L20.0026 8.49885L10.4287 0Z" fill="white"/>
  </g>
  <defs>
    <clipPath id="clip0_397_228">
      <rect width="19.496" height="17" fill="white" transform="translate(0.503906)"/>
    </clipPath>
  </defs>
</svg>
