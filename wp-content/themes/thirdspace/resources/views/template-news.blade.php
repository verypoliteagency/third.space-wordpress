{{--
  Template Name: News Template
--}}

@extends('layouts.app')

@section('content')
  @while( have_posts() )

    @php the_post() @endphp

    @if(!empty($featured_post))
      @include('partials.template-news-header')
    @else
      @include('partials.page-header')
    @endif

    <section class="section section--news-listing container-fluid">
      @include('partials.content-template-news')
    </section>

  @endwhile
@endsection
